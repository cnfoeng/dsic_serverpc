﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace DSIC_Server
{
    public partial class DBTest : DevExpress.XtraEditors.XtraForm
    {
        public DBTest(DataSet ds)
        {
            InitializeComponent();
            gridControl1.DataSource = ds.Tables[0];
        }
    }
}