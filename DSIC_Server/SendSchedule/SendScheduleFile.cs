﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Runtime.InteropServices;
using System.Net;
using System.IO;
using DevExpress.XtraRichEdit;
using DevExpress.XtraRichEdit.API.Native;

namespace DSIC_Server.SendSchedule
{
    public partial class SendScheduleFile : DevExpress.XtraEditors.XtraForm
    {
        public string networkPath = @"\\192.168.0.20\MapData"; // 디폴트말고 직접 만들고 공유 지정한 폴더여야함. PC이름 말고 반드시 IP여야함.
        NetworkCredential credentials = new NetworkCredential(@"{gowns}", "{hjpark12}");
        public string myNetworkPath = string.Empty;
        //public string ScheduleFilePath = Application.StartupPath + @"\Config\Schedule";
        string LocalFilePath = @"C:\SharedFolder\ScheduleFile";
        string NetworkDriveFilePath = @"SharedFolder\ScheduleFile";
        List<LocalNode> localNodes = new List<LocalNode>();

        bool bSendError = false;
        public SendScheduleFile()
        {
            InitializeComponent();
            InitCellList();

            cbFileType.SelectedIndex = 0;

            gridCellList.DataSource = localNodes;

            scFileList.Client = clbFileList;
        }
        List<LocalNode> InitCellList()
        {
            //사내 테스트용
            //localNodes.Add(new LocalNode(1, true, "test", "192.168.0.14", "gowns", "hjpark12"));
            //localNodes.Add(new LocalNode(2, true, "222", "192.168.0.20", "Administrator", "cnfoeng12!"));
            //localNodes.Add(new LocalNode(2, true, "222", "WIN-QSMCUADJ1LR", "Administrator", "cnfoeng12!"));

            localNodes.Add(new LocalNode(1, false, "1번셀", "10.32.88.161", "DS", ""));
            localNodes.Add(new LocalNode(2, false, "2번셀", "10.32.88.162", "DS2", ""));
            localNodes.Add(new LocalNode(3, false, "3번셀", "10.32.88.163", "abc", ""));
            localNodes.Add(new LocalNode(4, false, "4번셀", "10.32.88.164", "DI_EO", ""));
            localNodes.Add(new LocalNode(5, false, "5번셀", "10.32.88.165", "dieol", ""));
            localNodes.Add(new LocalNode(6, false, "6번셀", "10.32.88.166", "DS6", ""));

            return localNodes;
        }
        void MakeFileList(string FilePath)
        {
            tbScheduleFilePath.Text = FilePath;
            clbFileList.Items.Clear();

            DirectoryInfo d = new DirectoryInfo(FilePath);
            FileInfo[] Files = d.GetFiles("*.csv");

            var sortedFiles = new DirectoryInfo(FilePath).GetFiles()
                                                  .OrderBy(f => f.CreationTime)
                                                  .ToList();


            foreach (FileInfo file in sortedFiles)
            {
                clbFileList.Items.Add(file.Name);
            }
        }
        public async void FileUpload(string LocalFile, string networkPath, NetworkCredential credentials)
        {
            string myNetworkPath;
            try
            {
                string UploadURL = Path.GetFileName(LocalFile);
                using (new ConnectToSharedFolder(networkPath, credentials))
                {
                    byte[] file = File.ReadAllBytes(LocalFile);
                    myNetworkPath = networkPath + "\\" + UploadURL;

                    using (FileStream fileStream = File.Create(myNetworkPath, file.Length))
                    {
                        await fileStream.WriteAsync(file, 0, file.Length);
                        fileStream.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                string Message = ex.Message.ToString();
                bSendError = true;
            } 
        }
        public byte[] DownloadFileByte(string DownloadURL)
        {
            byte[] fileBytes = null;

            using (new ConnectToSharedFolder(networkPath, credentials))
            {
                var fileList = Directory.GetDirectories(networkPath);

                foreach (var item in fileList) { if (item.Contains("ClientDocuments")) { myNetworkPath = item; } }

                myNetworkPath = myNetworkPath + DownloadURL;

                try
                {
                    fileBytes = File.ReadAllBytes(myNetworkPath);
                }
                catch (Exception ex)
                {
                    string Message = ex.Message.ToString();
                }
            }

            return fileBytes;
        }

        private void btnFileSend_Click(object sender, EventArgs e)
        {
            if(clbFileList.CheckedItems.Count == 0)
            {
                AddLog("선택된" + cbFileType.Text + "이 없습니다.");
                return;
            }
            AddLog(cbFileType.Text + " 전송 시작.");
            try
            {
                foreach (LocalNode node in localNodes)
                {
                    if (node.Enable)
                    {
                        for (int i = 0; i < clbFileList.CheckedItems.Count; i++)
                        {
                            string fileName = LocalFilePath + "\\" + clbFileList.CheckedItems[i].ToString();
                            bSendError = false;
                            FileUpload(fileName, $"\\\\{node.IPAddress}\\"+NetworkDriveFilePath, new NetworkCredential($"{{{node.AccountID}}}", $"{{{node.AccountPWD}}}"));
                            if(!bSendError)
                            AddLog($"{node.CellName}({node.IPAddress})에  {clbFileList.CheckedItems[i].ToString()} 전송 완료.");
                            else
                            AddLog($"{node.CellName}({node.IPAddress})에  {clbFileList.CheckedItems[i].ToString()} 전송 실패.");
                        }
                    }
                }

                AddLog(cbFileType.Text + " 전송 완료.");
            }
            catch (Win32Exception e1)
            {
                AddLog(cbFileType.Text + " 전송 중 에러 발생. Error Message : " + e1.ToString());
            }
        }
        private void AddLog(string msg)
        {
            DocumentPosition pos = reLog.Document.CaretPosition;
            SubDocument doc = pos.BeginUpdateDocument();
            doc.InsertText(pos, msg+"\n");
            pos.EndUpdateDocument(doc);

            reLog.Document.CaretPosition = reLog.Document.Range.End;
            reLog.ScrollToCaret();
        }
        private void btnListUpdate_Click(object sender, EventArgs e)
        {
            AddLog("List 업데이트됨.");
            MakeFileList(LocalFilePath);
        }

        private void cbFileType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cbFileType.SelectedIndex == 0)//Schedule
            {
                LocalFilePath = @"C:\SharedFolder\ScheduleFile";
                NetworkDriveFilePath = @"SharedFolder\ScheduleFile";
                MakeFileList(LocalFilePath);
            }
            else if (cbFileType.SelectedIndex == 1)//Map
            {
                LocalFilePath = @"C:\SharedFolder\MapFile";
                NetworkDriveFilePath = @"SharedFolder\MapFile";
                MakeFileList(LocalFilePath);
            }
            else if (cbFileType.SelectedIndex == 2)//Fault List
            {
                LocalFilePath = @"C:\SharedFolder\FaultListFile";
                NetworkDriveFilePath = @"SharedFolder\FaultListFile";
                MakeFileList(LocalFilePath);
            }
            else if (cbFileType.SelectedIndex == 3)//Measuring
            {
                LocalFilePath = @"C:\SharedFolder\MeasuringFile";
                NetworkDriveFilePath = @"SharedFolder\MeasuringFile";
                MakeFileList(LocalFilePath);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }

    public class ConnectToSharedFolder : IDisposable
    {
        readonly string _networkName;

        public ConnectToSharedFolder(string networkName, NetworkCredential credentials)
        {
            _networkName = networkName;

            var netResource = new NetResource
            {
                Scope = ResourceScope.GlobalNetwork,
                ResourceType = ResourceType.Disk,
                DisplayType = ResourceDisplaytype.Share,
                RemoteName = networkName
            };

            var userName = string.IsNullOrEmpty(credentials.Domain)
                ? credentials.UserName
                : string.Format(@"{0}\{1}", credentials.Domain, credentials.UserName);

            var result = WNetAddConnection2(
                netResource,
                credentials.Password,
                userName,
                0);

            if (result != 0)
            {
                throw new Win32Exception(result, "Error connecting to remote share");

            }
        }

        ~ConnectToSharedFolder()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            WNetCancelConnection2(_networkName, 0, true);
        }

        [DllImport("mpr.dll")]
        private static extern int WNetAddConnection2(NetResource netResource,
            string password, string username, int flags);

        [DllImport("mpr.dll")]
        private static extern int WNetCancelConnection2(string name, int flags,
            bool force);

        [StructLayout(LayoutKind.Sequential)]
        public class NetResource
        {
            public ResourceScope Scope;
            public ResourceType ResourceType;
            public ResourceDisplaytype DisplayType;
            public int Usage;
            public string LocalName;
            public string RemoteName;
            public string Comment;
            public string Provider;
        }

        public enum ResourceScope : int
        {
            Connected = 1,
            GlobalNetwork,
            Remembered,
            Recent,
            Context
        };

        public enum ResourceType : int
        {
            Any = 0,
            Disk = 1,
            Print = 2,
            Reserved = 8,
        }

        public enum ResourceDisplaytype : int
        {
            Generic = 0x0,
            Domain = 0x01,
            Server = 0x02,
            Share = 0x03,
            File = 0x04,
            Group = 0x05,
            Network = 0x06,
            Root = 0x07,
            Shareadmin = 0x08,
            Directory = 0x09,
            Tree = 0x0a,
            Ndscontainer = 0x0b
        }


    }
}