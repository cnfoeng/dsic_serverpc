﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSIC_Server.SendSchedule
{
    public class LocalNode
    {
        int index;
        bool enable;
        string cellName;
        string iPAddress;
        string accountID;
        string accountPWD;

        public int Index { get => index; set => index = value; }
        public bool Enable { get => enable; set => enable = value; }
        public string CellName { get => cellName; set => cellName = value; }
        public string IPAddress { get => iPAddress; set => iPAddress = value; }
        public string AccountID { get => accountID; set => accountID = value; }
        public string AccountPWD { get => accountPWD; set => accountPWD = value; }

        public LocalNode(int index, bool enable, string name, string ipAddress, string accountID, string accountPWD)
        {
            this.index = index;
            this.enable = enable;
            this.cellName = name;
            this.iPAddress = ipAddress;
            this.accountID = accountID;
            this.accountPWD = accountPWD;
        }
    }
}
