﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace DSIC_Server.WriteEngineInfo
{
    public partial class WriteEngineInfomation : DevExpress.XtraEditors.XtraForm
    {
        DataSet dataSet;

        public WriteEngineInfomation()
        {
            InitializeComponent();
            SelectEngineInfo();
        }

        void SelectEngineInfo()
        {
            Tuple<DataSet, string> result = Main.database.DBSelectEngineInfo();

            if (result != null)
            {
                gridEngineInfo.BeginUpdate();
                gridEngineInfo.DataSource = result.Item1;
                dataSet = result.Item1;
                gridEngineInfo.DataMember = result.Item2;
                gridEngineInfo.EndUpdate();
            }
            else
            {
                XtraMessageBox.Show("<size=14>DB 쿼리 에러 발생</size>", "<size=14>에러</size>", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        void DeleteEngineInformation(string suffix, string mapFileName)
        {
            if (Main.database.DeleteEngineInfo(suffix, mapFileName))
            {
                SelectEngineInfo();
            }
            else
            {
                XtraMessageBox.Show("<size=14>DB 쿼리 에러 발생.</size>", "<size=14>에러</size>", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        void EditEngineInformation()
        {
            int selectedRow = gridView1.FocusedRowHandle;
            var a = dataSet.Tables[0].Rows[selectedRow].ItemArray;

            AddOrEditEngineInformation form = new AddOrEditEngineInformation(a);
            form.ShowDialog();

            SelectEngineInfo();
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddOrEditEngineInformation form = new AddOrEditEngineInformation();
            form.ShowDialog();

            SelectEngineInfo();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            EditEngineInformation();
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            int selectedRow = gridView1.FocusedRowHandle;
            var a = dataSet.Tables[0].Rows[selectedRow].ItemArray;

            DeleteEngineInformation(a[0].ToString(), a[7].ToString());
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            EditEngineInformation();
        }
    }
}