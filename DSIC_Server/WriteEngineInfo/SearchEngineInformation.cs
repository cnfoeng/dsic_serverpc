﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.IO;

namespace DSIC_Server.WriteEngineInfo
{

    public partial class SearchEngineInformation : DevExpress.XtraEditors.XtraForm
    {
        string ScheduleFilePath = @"C:\\SharedFolder\ScheduleFile";

        public SearchEngineInformation()
        {
            InitializeComponent();

            deStart.DateTime = DateTime.Now.AddMonths(-1);
            deEnd.DateTime = DateTime.Now;
            
            DirectoryInfo d = new DirectoryInfo(ScheduleFilePath);
            FileInfo[] Files = d.GetFiles("*.csv");

            var sortedFiles = new DirectoryInfo(ScheduleFilePath).GetFiles()
                                                  .OrderBy(f => f.CreationTime)
                                                  .ToList();

            cbScheduleFile.Properties.Items.Clear();

            foreach (FileInfo file in sortedFiles)
            {
                cbScheduleFile.Properties.Items.Add(file.Name);
            }

            cbModel.SelectedIndex = 0;
            
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            DateTime end = deEnd.DateTime.AddDays(+1);
            string strEnd = end.ToString("yyyy-MM-dd");
            Tuple<DataSet, string> result = Main.database.SearchDBResult(tbSuffix.Text, cbModel.Text, deStart.Text, strEnd, cbScheduleFile.Text);

            if (result != null)
            {
                gridControl1.BeginUpdate();
                gridControl1.DataSource = result.Item1;
                gridControl1.DataMember = result.Item2;
                gridControl1.EndUpdate();
            }
            else
            {
                XtraMessageBox.Show("<size=14>DB 쿼리 에러 발생</size>", "<size=14>에러</size>", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnReport_Click(object sender, EventArgs e)
        {
            
            if (gridView1.SelectedRowsCount > 1 || gridView1.FocusedRowHandle < 0)
            {
                XtraMessageBox.Show("<size=14>보고서 생성을 위해서는 한 개의 Row를 선택해주세요.</size>", "<size=14>에러</size>", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            string EGNO = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, gridView1.Columns["엔진번호"]).ToString();
            

            DataRow dr = Main.database.SearchDBResultByEGNOforResultReport(EGNO);

            ResultReport form = new ResultReport(dr);
            form.Show();
        }
        

        private void btnResultDataAnalization_Click(object sender, EventArgs e)
        {
            
            Tuple<DataSet, string> result = Main.database.SearchDBResult(tbSuffix.Text, cbModel.Text, deStart.Text, deEnd.Text, cbScheduleFile.Text);

            if (result != null)
            {
                ResultDataAnalization form = new ResultDataAnalization(result);
                form.Show();
            }
            
        }
    }
}