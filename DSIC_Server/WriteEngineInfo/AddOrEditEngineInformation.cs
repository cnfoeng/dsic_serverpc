﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.IO;

namespace DSIC_Server.WriteEngineInfo
{
    public partial class AddOrEditEngineInformation : DevExpress.XtraEditors.XtraForm
    {
        bool editMode = false;
        public AddOrEditEngineInformation()
        {
            InitializeComponent();

            cbUsage.SelectedIndex = 0;
            cbEngineType.SelectedIndex = 0;
            cbEngineModel.SelectedIndex = 0;
            cbEngineCategory.SelectedIndex = 0;
            cbTurboCharger.SelectedIndex = 0;

            cbGenerationDate.Text = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");

            editMode = false;
        }
        public AddOrEditEngineInformation(object[] itemArrays)
        {
            InitializeComponent();

            editMode = true;

            tbSuffix.ReadOnly = true;
            tbSuffix.Text = itemArrays[0].ToString();
            cbUsage.ReadOnly = true;
            cbUsage.Text = "YES";
            cbEngineCategory.Text = itemArrays[2].ToString();
            cbEngineType.Text = itemArrays[3].ToString();
            cbEngineModel.Text = itemArrays[1].ToString();
            tbScheduleFile.Text = itemArrays[8].ToString();
            tbMapFilePath.ReadOnly = true;
            tbMapFilePath.Text = itemArrays[7].ToString();
            tbEngineDiagnosticFilePath.Text = itemArrays[5].ToString();
            tbEngineMeasuringFilePath.Text = itemArrays[6].ToString();
            cbGenerationDate.ReadOnly = true;
            cbGenerationDate.Text = itemArrays[4].ToString();
            cbTurboCharger.Text = ((bool)itemArrays[10] == true) ? "YES" : "NO";
            tbForWhat.Text = itemArrays[9].ToString();
            tbDescription.Text = itemArrays[11].ToString();
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            if (editMode == false) // New
            {
                if(tbSuffix.Text == "" || tbMapFilePath.Text == "")
                {
                    XtraMessageBox.Show("<size=14>Suffix, Map File은 기준 정보이기에 반드시 입력되어야 합니다.</size>", "<size=14>에러</size>", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                if (Main.database.InsertEngineInfo(tbSuffix.Text, cbEngineModel.Text, cbEngineCategory.Text, cbEngineType.Text, cbGenerationDate.Text, tbEngineMeasuringFilePath.Text, tbEngineDiagnosticFilePath.Text, tbMapFilePath.Text, tbScheduleFile.Text, tbForWhat.Text, cbTurboCharger.Text, tbDescription.Text))
                {
                    this.Close();
                }
                else
                {
                    XtraMessageBox.Show("<size=14>DB 쿼리 에러 발생 또는 중복된 Suffix, Map File이 이미 존재합니다.</size>", "<size=14>에러</size>", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else // Edit
            {
                if (Main.database.UpdateEngineInfo(tbSuffix.Text, cbEngineModel.Text, cbEngineCategory.Text, cbEngineType.Text, cbGenerationDate.Text, tbEngineMeasuringFilePath.Text, tbEngineDiagnosticFilePath.Text, tbMapFilePath.Text, tbScheduleFile.Text, tbForWhat.Text, cbTurboCharger.Text, tbDescription.Text))
                {
                    this.Close();
                }
                else
                {
                    XtraMessageBox.Show("<size=14>DB 쿼리 에러 발생 또는 중복된 Suffix, Map File이 이미 존재합니다.</size>", "<size=14>에러</size>", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void AddOrEditEngineInformation_Load(object sender, EventArgs e)
        {
            
        }

        private void btnAddSchedule_Click(object sender, EventArgs e)
        {
            xtraOpenFileDialog1.InitialDirectory = @"C:\SharedFolder";
            xtraOpenFileDialog1.FileName = string.Empty;
            xtraOpenFileDialog1.ShowDialog();
            string fileName = xtraOpenFileDialog1.FileName;

            if (fileName == "")
            {
                return;
            }
            string str = Path.GetExtension(fileName);
            if (Path.GetExtension(fileName) != ".csv")
            {
                XtraMessageBox.Show("<size=14>파일 확장자가 지정된 확장자가 아닙니다.</size>", "<size=14>에러</size>", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            string fileNameOnly = Path.GetFileName(fileName);

            tbScheduleFile.Text = fileNameOnly;

        }

        private void btnAddHex_Click(object sender, EventArgs e)
        {
            xtraOpenFileDialog1.InitialDirectory = @"C:\SharedFolder";
            xtraOpenFileDialog1.FileName = string.Empty;
            xtraOpenFileDialog1.ShowDialog();
            string fileName = xtraOpenFileDialog1.FileName;


            if (fileName == "")
            {
                return;
            }

            if (Path.GetExtension(fileName) != ".hex")
            {
                XtraMessageBox.Show("<size=14>파일 확장자가 지정된 확장자가 아닙니다.</size>", "<size=14>에러</size>", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }



            string fileNameOnly = Path.GetFileName(fileName);

            string fileNameOnlyWithoutExtension = Path.GetFileNameWithoutExtension(fileName);

            string last2 = fileNameOnlyWithoutExtension.Substring(fileNameOnlyWithoutExtension.Length - 2, 2);
            bool isDynoMapCondition1 = (last2 == "_D") ? true : false;
            bool isDynoMapCondition2 = fileNameOnlyWithoutExtension.Contains("dyno");
            bool isDynoMapCondition3 = fileNameOnlyWithoutExtension.Contains("Dyno");
            bool isDynoMapCondition4 = fileNameOnlyWithoutExtension.Contains("DYNO");

            if (isDynoMapCondition1 || isDynoMapCondition2 || isDynoMapCondition3 || isDynoMapCondition4)
            {
                XtraMessageBox.Show("<size=14>출하용 맵을 선택해주세요.</size>", "<size=14>에러</size>", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }


            tbMapFilePath.Text = fileNameOnly;

        }

        private void btnAddEngineInfo_Click(object sender, EventArgs e)
        {
            xtraOpenFileDialog1.InitialDirectory = @"C:\SharedFolder";
            xtraOpenFileDialog1.FileName = string.Empty;
            xtraOpenFileDialog1.ShowDialog();
            string fileName = xtraOpenFileDialog1.FileName;
            if (fileName == "")
            {
                return;
            }

            if (Path.GetExtension(fileName) != ".csv")
            {
                XtraMessageBox.Show("<size=14>파일 확장자가 지정된 확장자가 아닙니다.</size>", "<size=14>에러</size>", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            string fileNameOnly = Path.GetFileName(fileName);

            tbEngineMeasuringFilePath.Text = fileNameOnly;

        }

        private void btnDiagnostics_Click(object sender, EventArgs e)
        {
            xtraOpenFileDialog1.InitialDirectory = @"C:\SharedFolder";
            xtraOpenFileDialog1.FileName = string.Empty;
            xtraOpenFileDialog1.ShowDialog();
            string fileName = xtraOpenFileDialog1.FileName;
            
            if (fileName == "")
            {
                return;
            }

            if (Path.GetExtension(fileName) != ".xlsx")
            {
                XtraMessageBox.Show("<size=14>파일 확장자가 지정된 확장자가 아닙니다.</size>", "<size=14>에러</size>", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            string fileNameOnly = Path.GetFileName(fileName);

            tbEngineDiagnosticFilePath.Text = fileNameOnly;

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        
    }
}