﻿namespace DSIC_Server.WriteEngineInfo
{
    partial class WriteEngineInfomation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WriteEngineInfomation));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnEdit = new DevExpress.XtraEditors.SimpleButton();
            this.btnRemove = new DevExpress.XtraEditors.SimpleButton();
            this.btnAdd = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.gridEngineInfo = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.ColSuffix = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColModel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColEngineCategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColEngineType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColGenerationDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColEngineConfigFilePath = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColDiagnosticsFilePath = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColFlashingFilePath = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColScheduleFilePath = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColForWhat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColTurboCharger = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColUsage = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridEngineInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.btnEdit);
            this.panelControl1.Controls.Add(this.btnRemove);
            this.panelControl1.Controls.Add(this.btnAdd);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1224, 65);
            this.panelControl1.TabIndex = 0;
            // 
            // btnEdit
            // 
            this.btnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEdit.Appearance.Options.UseFont = true;
            this.btnEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnEdit.ImageOptions.Image")));
            this.btnEdit.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnEdit.Location = new System.Drawing.Point(272, 14);
            this.btnEdit.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(107, 37);
            this.btnEdit.TabIndex = 5;
            this.btnEdit.Text = "편집";
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnRemove
            // 
            this.btnRemove.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRemove.Appearance.Options.UseFont = true;
            this.btnRemove.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnRemove.ImageOptions.Image")));
            this.btnRemove.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnRemove.Location = new System.Drawing.Point(142, 14);
            this.btnRemove.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(107, 37);
            this.btnRemove.TabIndex = 4;
            this.btnRemove.Text = "제거";
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.Appearance.Options.UseFont = true;
            this.btnAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.ImageOptions.Image")));
            this.btnAdd.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnAdd.Location = new System.Drawing.Point(12, 14);
            this.btnAdd.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(107, 37);
            this.btnAdd.TabIndex = 3;
            this.btnAdd.Text = "추가";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.gridEngineInfo);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 65);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1224, 584);
            this.panelControl2.TabIndex = 1;
            // 
            // gridEngineInfo
            // 
            this.gridEngineInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridEngineInfo.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridEngineInfo.Location = new System.Drawing.Point(2, 2);
            this.gridEngineInfo.MainView = this.gridView1;
            this.gridEngineInfo.Name = "gridEngineInfo";
            this.gridEngineInfo.Size = new System.Drawing.Size(1220, 580);
            this.gridEngineInfo.TabIndex = 1;
            this.gridEngineInfo.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView1.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView1.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView1.Appearance.Row.Options.UseTextOptions = true;
            this.gridView1.Appearance.Row.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.ColSuffix,
            this.ColModel,
            this.ColEngineCategory,
            this.ColEngineType,
            this.ColGenerationDate,
            this.ColEngineConfigFilePath,
            this.ColDiagnosticsFilePath,
            this.ColFlashingFilePath,
            this.ColScheduleFilePath,
            this.ColForWhat,
            this.ColTurboCharger,
            this.ColUsage});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFullFocus;
            this.gridView1.GridControl = this.gridEngineInfo;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.OptionsFilter.AllowFilterEditor = false;
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsFind.Behavior = DevExpress.XtraEditors.FindPanelBehavior.Search;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            // 
            // ColSuffix
            // 
            this.ColSuffix.Caption = "Suffix";
            this.ColSuffix.FieldName = "Suffix";
            this.ColSuffix.Name = "ColSuffix";
            this.ColSuffix.OptionsFilter.AllowAutoFilter = false;
            this.ColSuffix.OptionsFilter.AllowFilter = false;
            this.ColSuffix.Visible = true;
            this.ColSuffix.VisibleIndex = 0;
            this.ColSuffix.Width = 139;
            // 
            // ColModel
            // 
            this.ColModel.Caption = "Model";
            this.ColModel.FieldName = "Model";
            this.ColModel.Name = "ColModel";
            this.ColModel.OptionsFilter.AllowAutoFilter = false;
            this.ColModel.OptionsFilter.AllowFilter = false;
            this.ColModel.Visible = true;
            this.ColModel.VisibleIndex = 1;
            this.ColModel.Width = 96;
            // 
            // ColEngineCategory
            // 
            this.ColEngineCategory.Caption = "엔진 구분";
            this.ColEngineCategory.FieldName = "CategoryLarge";
            this.ColEngineCategory.Name = "ColEngineCategory";
            this.ColEngineCategory.OptionsFilter.AllowAutoFilter = false;
            this.ColEngineCategory.OptionsFilter.AllowFilter = false;
            this.ColEngineCategory.Visible = true;
            this.ColEngineCategory.VisibleIndex = 2;
            this.ColEngineCategory.Width = 104;
            // 
            // ColEngineType
            // 
            this.ColEngineType.Caption = "엔진 형태";
            this.ColEngineType.FieldName = "CategorySmall";
            this.ColEngineType.Name = "ColEngineType";
            this.ColEngineType.OptionsFilter.AllowAutoFilter = false;
            this.ColEngineType.OptionsFilter.AllowFilter = false;
            this.ColEngineType.Visible = true;
            this.ColEngineType.VisibleIndex = 3;
            this.ColEngineType.Width = 110;
            // 
            // ColGenerationDate
            // 
            this.ColGenerationDate.Caption = "생성일";
            this.ColGenerationDate.FieldName = "DateCreate";
            this.ColGenerationDate.Name = "ColGenerationDate";
            this.ColGenerationDate.OptionsFilter.AllowAutoFilter = false;
            this.ColGenerationDate.OptionsFilter.AllowFilter = false;
            this.ColGenerationDate.Visible = true;
            this.ColGenerationDate.VisibleIndex = 4;
            this.ColGenerationDate.Width = 157;
            // 
            // ColEngineConfigFilePath
            // 
            this.ColEngineConfigFilePath.Caption = "엔진 정보 환경 설정";
            this.ColEngineConfigFilePath.FieldName = "MeasuringFilePath";
            this.ColEngineConfigFilePath.Name = "ColEngineConfigFilePath";
            this.ColEngineConfigFilePath.OptionsFilter.AllowAutoFilter = false;
            this.ColEngineConfigFilePath.OptionsFilter.AllowFilter = false;
            this.ColEngineConfigFilePath.Visible = true;
            this.ColEngineConfigFilePath.VisibleIndex = 5;
            this.ColEngineConfigFilePath.Width = 250;
            // 
            // ColDiagnosticsFilePath
            // 
            this.ColDiagnosticsFilePath.Caption = "엔진 진단 환경 설정";
            this.ColDiagnosticsFilePath.FieldName = "FaultListFilePath";
            this.ColDiagnosticsFilePath.Name = "ColDiagnosticsFilePath";
            this.ColDiagnosticsFilePath.OptionsFilter.AllowAutoFilter = false;
            this.ColDiagnosticsFilePath.OptionsFilter.AllowFilter = false;
            this.ColDiagnosticsFilePath.Visible = true;
            this.ColDiagnosticsFilePath.VisibleIndex = 6;
            this.ColDiagnosticsFilePath.Width = 250;
            // 
            // ColFlashingFilePath
            // 
            this.ColFlashingFilePath.Caption = "Flashing 환경 설정";
            this.ColFlashingFilePath.FieldName = "ECUMapFilePath";
            this.ColFlashingFilePath.Name = "ColFlashingFilePath";
            this.ColFlashingFilePath.OptionsFilter.AllowAutoFilter = false;
            this.ColFlashingFilePath.OptionsFilter.AllowFilter = false;
            this.ColFlashingFilePath.Visible = true;
            this.ColFlashingFilePath.VisibleIndex = 7;
            this.ColFlashingFilePath.Width = 250;
            // 
            // ColScheduleFilePath
            // 
            this.ColScheduleFilePath.Caption = "Schedule";
            this.ColScheduleFilePath.FieldName = "ScheduleFilePath";
            this.ColScheduleFilePath.Name = "ColScheduleFilePath";
            this.ColScheduleFilePath.OptionsFilter.AllowAutoFilter = false;
            this.ColScheduleFilePath.OptionsFilter.AllowFilter = false;
            this.ColScheduleFilePath.Visible = true;
            this.ColScheduleFilePath.VisibleIndex = 8;
            this.ColScheduleFilePath.Width = 250;
            // 
            // ColForWhat
            // 
            this.ColForWhat.Caption = "용도";
            this.ColForWhat.FieldName = "Purpose";
            this.ColForWhat.Name = "ColForWhat";
            this.ColForWhat.OptionsFilter.AllowAutoFilter = false;
            this.ColForWhat.OptionsFilter.AllowFilter = false;
            this.ColForWhat.Visible = true;
            this.ColForWhat.VisibleIndex = 9;
            this.ColForWhat.Width = 250;
            // 
            // ColTurboCharger
            // 
            this.ColTurboCharger.Caption = "터보차저";
            this.ColTurboCharger.FieldName = "TurboCharger";
            this.ColTurboCharger.Name = "ColTurboCharger";
            this.ColTurboCharger.OptionsFilter.AllowAutoFilter = false;
            this.ColTurboCharger.OptionsFilter.AllowFilter = false;
            this.ColTurboCharger.Visible = true;
            this.ColTurboCharger.VisibleIndex = 10;
            this.ColTurboCharger.Width = 105;
            // 
            // ColUsage
            // 
            this.ColUsage.Caption = "사용여부";
            this.ColUsage.FieldName = "Usage";
            this.ColUsage.Name = "ColUsage";
            this.ColUsage.OptionsFilter.AllowAutoFilter = false;
            this.ColUsage.OptionsFilter.AllowFilter = false;
            this.ColUsage.Visible = true;
            this.ColUsage.VisibleIndex = 11;
            this.ColUsage.Width = 102;
            // 
            // WriteEngineInfomation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1224, 649);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "WriteEngineInfomation";
            this.Text = "Engine Infomation";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridEngineInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraGrid.GridControl gridEngineInfo;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn ColSuffix;
        private DevExpress.XtraGrid.Columns.GridColumn ColModel;
        private DevExpress.XtraEditors.SimpleButton btnEdit;
        private DevExpress.XtraEditors.SimpleButton btnRemove;
        private DevExpress.XtraEditors.SimpleButton btnAdd;
        private DevExpress.XtraGrid.Columns.GridColumn ColEngineCategory;
        private DevExpress.XtraGrid.Columns.GridColumn ColEngineType;
        private DevExpress.XtraGrid.Columns.GridColumn ColGenerationDate;
        private DevExpress.XtraGrid.Columns.GridColumn ColEngineConfigFilePath;
        private DevExpress.XtraGrid.Columns.GridColumn ColDiagnosticsFilePath;
        private DevExpress.XtraGrid.Columns.GridColumn ColFlashingFilePath;
        private DevExpress.XtraGrid.Columns.GridColumn ColScheduleFilePath;
        private DevExpress.XtraGrid.Columns.GridColumn ColForWhat;
        private DevExpress.XtraGrid.Columns.GridColumn ColTurboCharger;
        private DevExpress.XtraGrid.Columns.GridColumn ColUsage;
    }
}