﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DevExpress.UserSkins;
using DevExpress.Skins;
using DevExpress.LookAndFeel;
using System.Threading;
using DevExpress.XtraEditors;
using System.Diagnostics;

namespace DSIC_Server
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = true;

            bool bnew;
            Mutex mutex = new Mutex(true, "MutexName", out bnew);
            if (bnew)
            {
                Process.GetCurrentProcess().PriorityBoostEnabled = true;
                Process.GetCurrentProcess().PriorityClass = ProcessPriorityClass.High;

                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new Login());
                mutex.ReleaseMutex();
            }
            else
            {
                XtraMessageBox.Show("<size=14>프로그램이 이미 실행 중입니다.</size>", "<size=14>에러</size>", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();

            }
        }
    }
}
