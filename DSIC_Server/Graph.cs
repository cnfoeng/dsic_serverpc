﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraCharts;

namespace DSIC_Server
{
    public partial class Graph : DevExpress.XtraEditors.XtraForm
    {
        DataTable selectedDataTable = new DataTable();

        public Graph(Tuple<DataSet, string> tuple, int[] selectedRows)
        {
            InitializeComponent();

            string[] columnNames = tuple.Item1.Tables[0].Columns.Cast<DataColumn>()
                                                                .Select(x => x.ColumnName)
                                                                .ToArray();
            selectedDataTable = tuple.Item1.Tables[0].Clone();
            selectedDataTable.Rows.Clear();

            for (int i = 0; i < selectedRows.Length; i++)
            {

                DataRow dr = tuple.Item1.Tables[0].Rows[selectedRows[i]];
                selectedDataTable.ImportRow(dr);
            }
            cbColumnNames.Properties.Items.AddRange(columnNames);

            string dataColumnName = "최대출력 RPM 측정값";
            cbColumnNames.Text = dataColumnName;
            SetChartData(chartControl1, ViewType.Bar, selectedDataTable, dataColumnName);
            InputValue(selectedDataTable, dataColumnName);
        }

        private void SetChartData(ChartControl chart, ViewType viewType, DataTable dt, string dataColumnName)
        {
            #region HorizontalBarChart
            if (viewType == ViewType.Bar)
            {
                Dictionary<string, Series> seriesList = new Dictionary<string, Series>();
                chart.Series.Clear();
                foreach (DataRow row in dt.Rows)
                {
                    string product = row["엔진번호"].ToString();
                    string generationDate = row["시험날짜"].ToString();
                    string year = row[dataColumnName].ToString();

                    Series series;

                    if (seriesList.TryGetValue(product, out series) == false)
                    {
                        seriesList.Add(product, series = new Series(product, viewType));
                        series.LabelsVisibility = DevExpress.Utils.DefaultBoolean.True;
                        chart.Series.Add(series);
                    }

                   ((XYDiagram)chart.Diagram).Rotated = true;
                    try
                    {
                        SeriesPoint point = new SeriesPoint(generationDate, year);
                        series.Points.Add(point);
                    }
                    catch (FormatException)
                    {
                        XtraMessageBox.Show("<size=14>그래프화 할 수 없는 데이터 형식입니다.</size>", "<size=14>에러</size>", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        chart.Series.Clear();
                        return;
                    }
                }
            }
            #endregion
        }

        private void InputValue(DataTable dt, string dataColumnName)
        {
            double min = 0;
            double max = 0;
            double mean = 0;

            int sumCount = 0;

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                double value = 0;

                if(dt.Rows[i][dataColumnName] is string)
                {
                    try
                    {
                        value = double.Parse((string)dt.Rows[i][dataColumnName]);
                    }
                    catch (FormatException)
                    {
                        break;
                    }
                }
                else if(dt.Rows[i][dataColumnName] is double)
                { 
                    value = dt.Rows[i].Field<double>(dataColumnName);
                }
                else
                {
                    break;
                }

                if (i == 0)
                {
                    min = value;
                    max = value;
                    mean = value;
                    sumCount++;
                    continue;
                }

                if (min > value)
                {
                    min = value;
                }
                if(max < value)
                {
                    max = value;
                }
                mean += value;

                sumCount++;
            }

            mean = mean / sumCount;

            tbMin.Text = min.ToString();
            tbMax.Text = max.ToString();
            tbMean.Text = mean.ToString();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cbColumnNames_SelectedIndexChanged(object sender, EventArgs e)
        {
            string dataColumnName = cbColumnNames.Text;
            SetChartData(chartControl1, ViewType.Bar, selectedDataTable, dataColumnName);
            InputValue(selectedDataTable, dataColumnName);
        }
    }
}