﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.LookAndFeel;
using DevExpress.Skins;
namespace DSIC_Server
{
    public partial class Login : DevExpress.XtraEditors.XtraForm
    {
        public Login()
        {
            InitializeComponent();
            
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if(tbUsername.Text == "a" && tbPassword.Text == "a")
            {
                this.Hide();
                var form = new Main();
                form.Closed += (s, args) => this.Close();
                form.Show();
            }
            else
            {
                XtraMessageBox.Show("<size=14>사용자 명 또는 비밀번호가 틀렸습니다.</size>", "<size=14>에러</size>", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
