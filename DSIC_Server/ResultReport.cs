﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.Spreadsheet;

namespace DSIC_Server
{
    public partial class ResultReport : DevExpress.XtraEditors.XtraForm
    {

        public ResultReport(DataRow dr)
        {
            InitializeComponent();

            lbReportEGNO.Text = "Engine Number " + dr[0].ToString() + " Report";

            string defaultExcelFilePath = Application.StartupPath + "\\ResultReport.xlsx";
            spreadsheetControl1.LoadDocument(defaultExcelFilePath);
            
            InputCellValue(dr);
        }

        private void InputCellValue(DataRow dr)
        {
            Worksheet worksheet1 = spreadsheetControl1.Document.Worksheets[0];

            if (dr[0].ToString().Contains("DX22"))
            {
                worksheet1.Range["D14"].Value = "Torque";
            }

            worksheet1.Range["C4"].Value = dr[0].ToString();//EGNO
            worksheet1.Range["G4"].Value = dr[1].ToString();//MODEL
            worksheet1.Range["C5"].Value = dr[2].ToString();//ScheduleFilePath
            worksheet1.Range["G5"].Value = (bool)dr[3] ? "YES" : "NO";//TurboCharger
            worksheet1.Range["C6"].Value = dr[4].ToString();//ECUFileName

            worksheet1.Range["C10"].Value = dr[5].ToString();//AIRTEMP
            worksheet1.Range["E10"].Value = dr[6].ToString();//RELHUMID
            worksheet1.Range["G10"].Value = dr[7].ToString();//AIRPRE
            worksheet1.Range["C11"].Value = dr[8].ToString();//KNUM
            worksheet1.Range["E11"].Value = dr[9].ToString();//FTEMPIN
            worksheet1.Range["G11"].Value = dr[10].ToString();//WTEMP
            worksheet1.Range["C12"].Value = dr[11].ToString();//FuelDensity
            worksheet1.Range["E12"].Value = dr[12].ToString();//Coeff_fm

            worksheet1.Range["D15"].Value = dr[13].ToString();//APS_rpm
            worksheet1.Range["E15"].Value = dr[14].ToString();//APS_min
            worksheet1.Range["F15"].Value = dr[15].ToString();//APS_max
            worksheet1.Range["G15"].Value = dr[16].ToString();//APS
            if ((double)dr[16] >= double.Parse(dr[14].ToString()) && (double)dr[16] <= double.Parse(dr[15].ToString()))
            {
                worksheet1.Range["H15"].Value = "O";
                worksheet1.Range["H15"].Font.Color = Color.Blue;
            }
            else
            {
                worksheet1.Range["H15"].Value = "X";
                worksheet1.Range["H15"].Font.Color = Color.Red;
            }

            worksheet1.Range["D16"].Value = dr[17].ToString();//ATQ_rpm
            worksheet1.Range["E16"].Value = dr[18].ToString();//ATQ_min
            worksheet1.Range["F16"].Value = dr[19].ToString();//ATQ_max
            worksheet1.Range["G16"].Value = dr[20].ToString();//ATQ
            if ((double)dr[20] >= double.Parse(dr[18].ToString()) && (double)dr[20] <= double.Parse(dr[19].ToString()))
            {
                worksheet1.Range["H16"].Value = "O";
                worksheet1.Range["H16"].Font.Color = Color.Blue;
            }
            else
            {
                worksheet1.Range["H16"].Value = "X";
                worksheet1.Range["H16"].Font.Color = Color.Red;
            }

            worksheet1.Range["D18"].Value = dr[21].ToString();//ASMOK_rpm
            worksheet1.Range["F18"].Value = dr[22].ToString();//ASMOK_max
            worksheet1.Range["G18"].Value = dr[23].ToString();//ASMOK
            if ((double)dr[23] <= double.Parse(dr[22].ToString()))
            {
                worksheet1.Range["H18"].Value = "O";
                worksheet1.Range["H18"].Font.Color = Color.Blue;
            }
            else
            {
                worksheet1.Range["H18"].Value = "X";
                worksheet1.Range["H18"].Font.Color = Color.Red;
            }

            worksheet1.Range["F19"].Value = dr[24].ToString();//AFASMOK_max
            worksheet1.Range["G19"].Value = dr[25].ToString();//AFASMOK
            if ((double)dr[25] <= double.Parse(dr[24].ToString()))
            {
                worksheet1.Range["H19"].Value = "O";
                worksheet1.Range["H19"].Font.Color = Color.Blue;
            }
            else
            {
                worksheet1.Range["H19"].Value = "X";
                worksheet1.Range["H19"].Font.Color = Color.Red;
            }

            worksheet1.Range["E20"].Value = dr[26].ToString();//ALOILP_min
            worksheet1.Range["F20"].Value = dr[27].ToString();//ALOILP_max
            worksheet1.Range["G20"].Value = dr[28].ToString();//ALOILP
            if ((double)dr[28] >= double.Parse(dr[26].ToString()) && (double)dr[28] <= double.Parse(dr[27].ToString()))
            {
                worksheet1.Range["H20"].Value = "O";
                worksheet1.Range["H20"].Font.Color = Color.Blue;
            }
            else
            {
                worksheet1.Range["H20"].Value = "X";
                worksheet1.Range["H20"].Font.Color = Color.Red;
            }

            worksheet1.Range["E21"].Value = dr[29].ToString();//AFOILP_min
            worksheet1.Range["F21"].Value = dr[30].ToString();//AFOILP_max
            worksheet1.Range["G21"].Value = dr[31].ToString();//AFOILP
            if ((double)dr[31] >= double.Parse(dr[29].ToString()) && (double)dr[31] <= double.Parse(dr[30].ToString()))
            {
                worksheet1.Range["H21"].Value = "O";
                worksheet1.Range["H21"].Font.Color = Color.Blue;
            }
            else
            {
                worksheet1.Range["H21"].Value = "X";
                worksheet1.Range["H21"].Font.Color = Color.Red;
            }

            worksheet1.Range["E22"].Value = dr[32].ToString();//AJOILP_min
            worksheet1.Range["F22"].Value = dr[33].ToString();//AJOILP_max
            worksheet1.Range["G22"].Value = dr[34].ToString();//AJOILP
            if ((double)dr[34] >= double.Parse(dr[32].ToString()) && (double)dr[34] <= double.Parse(dr[33].ToString()))
            {
                worksheet1.Range["H22"].Value = "O";
                worksheet1.Range["H22"].Font.Color = Color.Blue;
            }
            else
            {
                worksheet1.Range["H22"].Value = "X";
                worksheet1.Range["H22"].Font.Color = Color.Red;
            }

            worksheet1.Range["E23"].Value = dr[35].ToString();//ALRPM_min
            worksheet1.Range["F23"].Value = dr[36].ToString();//ALRPM_max
            worksheet1.Range["G23"].Value = dr[37].ToString();//ALRPM
            if ((double)dr[37] >= double.Parse(dr[35].ToString()) && (double)dr[37] <= double.Parse(dr[36].ToString()))
            {
                worksheet1.Range["H23"].Value = "O";
                worksheet1.Range["H23"].Font.Color = Color.Blue;
            }
            else
            {
                worksheet1.Range["H23"].Value = "X";
                worksheet1.Range["H23"].Font.Color = Color.Red;
            }

            worksheet1.Range["E24"].Value = dr[38].ToString();//AFRPM_min
            worksheet1.Range["F24"].Value = dr[39].ToString();//AFRPM_max
            worksheet1.Range["G24"].Value = dr[40].ToString();//AFRPM
            if ((double)dr[40] >= double.Parse(dr[38].ToString()) && (double)dr[40] <= double.Parse(dr[39].ToString()))
            {
                worksheet1.Range["H24"].Value = "O";
                worksheet1.Range["H24"].Font.Color = Color.Blue;
            }
            else
            {
                worksheet1.Range["H24"].Value = "X";
                worksheet1.Range["H24"].Font.Color = Color.Red;
            }
            worksheet1.Range["D28"].Value = dr[41].ToString();//ABSFC_rpm
            worksheet1.Range["F28"].Value = dr[42].ToString();//ABSFC_max
            worksheet1.Range["G28"].Value = dr[43].ToString();//ABSFC
            if ((double)dr[43] <= double.Parse(dr[42].ToString()))
            {
                worksheet1.Range["H28"].Value = "O";
                worksheet1.Range["H28"].Font.Color = Color.Blue;
            }
            else
            {
                worksheet1.Range["H28"].Value = "X";
                worksheet1.Range["H28"].Font.Color = Color.Red;
            }

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}