﻿namespace DSIC_Server
{
    partial class Graph
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Graph));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.btnClose = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.tbMean = new DevExpress.XtraEditors.TextEdit();
            this.tbMin = new DevExpress.XtraEditors.TextEdit();
            this.tbMax = new DevExpress.XtraEditors.TextEdit();
            this.lbMean = new DevExpress.XtraEditors.LabelControl();
            this.lbMin = new DevExpress.XtraEditors.LabelControl();
            this.lbMax = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.cbColumnNames = new DevExpress.XtraEditors.ComboBoxEdit();
            this.chartControl1 = new DevExpress.XtraCharts.ChartControl();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbMean.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbMin.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbMax.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbColumnNames.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl1)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(995, 64);
            this.panelControl1.TabIndex = 0;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Location = new System.Drawing.Point(12, 15);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(154, 29);
            this.labelControl2.TabIndex = 3;
            this.labelControl2.Text = "데이터 그래프화";
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.btnClose);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl2.Location = new System.Drawing.Point(0, 480);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(995, 58);
            this.panelControl2.TabIndex = 1;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Appearance.Options.UseFont = true;
            this.btnClose.Location = new System.Drawing.Point(866, 9);
            this.btnClose.LookAndFeel.SkinName = "Office 2013 Dark Gray";
            this.btnClose.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(118, 40);
            this.btnClose.TabIndex = 0;
            this.btnClose.Text = "닫기";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.groupControl1);
            this.panelControl3.Controls.Add(this.labelControl1);
            this.panelControl3.Controls.Add(this.cbColumnNames);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelControl3.Location = new System.Drawing.Point(774, 64);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(221, 416);
            this.panelControl3.TabIndex = 2;
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.tbMean);
            this.groupControl1.Controls.Add(this.tbMin);
            this.groupControl1.Controls.Add(this.tbMax);
            this.groupControl1.Controls.Add(this.lbMean);
            this.groupControl1.Controls.Add(this.lbMin);
            this.groupControl1.Controls.Add(this.lbMax);
            this.groupControl1.Location = new System.Drawing.Point(7, 78);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(208, 227);
            this.groupControl1.TabIndex = 2;
            // 
            // tbMean
            // 
            this.tbMean.Location = new System.Drawing.Point(11, 183);
            this.tbMean.Name = "tbMean";
            this.tbMean.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbMean.Properties.Appearance.Options.UseFont = true;
            this.tbMean.Properties.ReadOnly = true;
            this.tbMean.Size = new System.Drawing.Size(150, 26);
            this.tbMean.TabIndex = 7;
            // 
            // tbMin
            // 
            this.tbMin.Location = new System.Drawing.Point(11, 122);
            this.tbMin.Name = "tbMin";
            this.tbMin.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbMin.Properties.Appearance.Options.UseFont = true;
            this.tbMin.Properties.ReadOnly = true;
            this.tbMin.Size = new System.Drawing.Size(150, 26);
            this.tbMin.TabIndex = 6;
            // 
            // tbMax
            // 
            this.tbMax.Location = new System.Drawing.Point(11, 61);
            this.tbMax.Name = "tbMax";
            this.tbMax.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbMax.Properties.Appearance.Options.UseFont = true;
            this.tbMax.Properties.ReadOnly = true;
            this.tbMax.Size = new System.Drawing.Size(150, 26);
            this.tbMax.TabIndex = 3;
            // 
            // lbMean
            // 
            this.lbMean.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbMean.Appearance.Options.UseFont = true;
            this.lbMean.Location = new System.Drawing.Point(11, 158);
            this.lbMean.Name = "lbMean";
            this.lbMean.Size = new System.Drawing.Size(42, 19);
            this.lbMean.TabIndex = 5;
            this.lbMean.Text = "평균값";
            // 
            // lbMin
            // 
            this.lbMin.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbMin.Appearance.Options.UseFont = true;
            this.lbMin.Location = new System.Drawing.Point(11, 97);
            this.lbMin.Name = "lbMin";
            this.lbMin.Size = new System.Drawing.Size(42, 19);
            this.lbMin.TabIndex = 4;
            this.lbMin.Text = "최소값";
            // 
            // lbMax
            // 
            this.lbMax.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbMax.Appearance.Options.UseFont = true;
            this.lbMax.Location = new System.Drawing.Point(11, 36);
            this.lbMax.Name = "lbMax";
            this.lbMax.Size = new System.Drawing.Size(42, 19);
            this.lbMax.TabIndex = 3;
            this.lbMax.Text = "최대값";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(10, 12);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(61, 19);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "칼럼 이름";
            // 
            // cbColumnNames
            // 
            this.cbColumnNames.Location = new System.Drawing.Point(7, 37);
            this.cbColumnNames.Name = "cbColumnNames";
            this.cbColumnNames.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbColumnNames.Properties.Appearance.Options.UseFont = true;
            this.cbColumnNames.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbColumnNames.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cbColumnNames.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbColumnNames.Size = new System.Drawing.Size(208, 26);
            this.cbColumnNames.TabIndex = 0;
            this.cbColumnNames.SelectedIndexChanged += new System.EventHandler(this.cbColumnNames_SelectedIndexChanged);
            // 
            // chartControl1
            // 
            this.chartControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartControl1.Legend.Name = "Default Legend";
            this.chartControl1.Location = new System.Drawing.Point(0, 64);
            this.chartControl1.Name = "chartControl1";
            this.chartControl1.SeriesSerializable = new DevExpress.XtraCharts.Series[0];
            this.chartControl1.Size = new System.Drawing.Size(774, 416);
            this.chartControl1.TabIndex = 3;
            // 
            // Graph
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(995, 538);
            this.Controls.Add(this.chartControl1);
            this.Controls.Add(this.panelControl3);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Graph";
            this.Text = "Graph";
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.panelControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbMean.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbMin.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbMax.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbColumnNames.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraCharts.ChartControl chartControl1;
        private DevExpress.XtraEditors.SimpleButton btnClose;
        private DevExpress.XtraEditors.ComboBoxEdit cbColumnNames;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.TextEdit tbMean;
        private DevExpress.XtraEditors.TextEdit tbMin;
        private DevExpress.XtraEditors.TextEdit tbMax;
        private DevExpress.XtraEditors.LabelControl lbMean;
        private DevExpress.XtraEditors.LabelControl lbMin;
        private DevExpress.XtraEditors.LabelControl lbMax;
    }
}