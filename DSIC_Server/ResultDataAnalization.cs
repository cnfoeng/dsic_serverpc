﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace DSIC_Server
{
    public partial class ResultDataAnalization : DevExpress.XtraEditors.XtraForm
    {
        Tuple<DataSet, string> tuple;

        public ResultDataAnalization(Tuple<DataSet, string> tuple)
        {
            InitializeComponent();

            gridControl1.DataSource = tuple.Item1.Tables[0];
            this.tuple = tuple;
            gridView1.BestFitColumns();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnGraph_Click(object sender, EventArgs e)
        {
            if (gridView1.FocusedRowHandle < 0)
            {
                XtraMessageBox.Show("<size=14>그래프 생성을 위해서는 한 개 이상의 Row를 선택해주세요.</size>", "<size=14>에러</size>", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            int[] selectedRows = gridView1.GetSelectedRows();

            Graph form = new Graph(tuple, selectedRows);
            form.Show();

        }
    }
}