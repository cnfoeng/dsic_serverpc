﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace DSIC_Server
{
    public partial class Settings : DevExpress.XtraEditors.XtraForm
    {
        public Settings()
        {
            InitializeComponent();

            tbIP.Text = Properties.Settings.Default.DBIPAddress;
            tbPort.Text = Properties.Settings.Default.DBPort;
            tbUserID.Text = Properties.Settings.Default.DBUserID;
            tbPwd.Text = Properties.Settings.Default.DBPwd;
            tbTableName.Text = Properties.Settings.Default.DBTableName;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Settings_FormClosed(object sender, FormClosedEventArgs e)
        {
            Properties.Settings.Default.DBIPAddress = tbIP.Text;
            Properties.Settings.Default.DBPort = tbPort.Text;
            Properties.Settings.Default.DBUserID = tbUserID.Text;
            Properties.Settings.Default.DBPwd = tbPwd.Text;
            Properties.Settings.Default.DBTableName = tbTableName.Text;

            Properties.Settings.Default.Save();
        }
    }
}