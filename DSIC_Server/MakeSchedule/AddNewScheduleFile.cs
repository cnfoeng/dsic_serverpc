﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace DSIC_Server.MakeSchedule
{
    public partial class AddNewScheduleFile : DevExpress.XtraEditors.XtraForm
    {
        public string FileName = "";
        public string Description = "";
        

        public AddNewScheduleFile(string fileName, string description)
        {
            InitializeComponent();

            this.FileName = fileName;
            this.Description = description;

            tbFileName.Text = FileName;
            tbDescription.Text = Description;
        }

        public AddNewScheduleFile()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            FileName = tbFileName.Text;
            Description = tbDescription.Text;
        }
    }
}