﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;
using System.IO;
using DevExpress.XtraGrid.Views.Grid;
using diag = System.Diagnostics;
namespace DSIC_Server.MakeSchedule
{
    public partial class MakeScheduleList : DevExpress.XtraEditors.XtraForm
    {
        List<ScheduleFile> scheduleFiles = new List<ScheduleFile>();
        ScheduleFile Copied = null;
        //string ScheduleFilePath = @".\Config\Schedule";
        string ScheduleFilePath = @"C:\\SharedFolder\ScheduleFile";
        public MakeScheduleList()
        {
            diag.Debug.Print("1");
            InitializeComponent();
            ListFileOnGridview();
            InitGridview();
            gridScheduleList.DataSource = scheduleFiles;
        }
        void InitGridview()
        {
            ContextMenuStrip cmGridview1 = new ContextMenuStrip();
            cmGridview1.Items.Add("스케쥴 추가", null, btnAdd_Click);
            cmGridview1.Items.Add("스케쥴 제거", null, btnRemove_Click);
            cmGridview1.Items.Add("스케쥴 편집", null, btnEdit_Click);
            cmGridview1.Items.Add(new ToolStripSeparator());
            cmGridview1.Items.Add("스케쥴 복사", null, cm_CopySchedule_Click);
            cmGridview1.Items.Add("스케쥴 붙여넣기", null, cm_PasteSchedule_Click);
            cmGridview1.Items[5].Enabled = false;
            gridScheduleList.ContextMenuStrip = cmGridview1;

            ContextMenuStrip cmGridview2 = new ContextMenuStrip();
            cmGridview2.Items.Add("스텝 추가", null, cm_AddStep_Click);
            cmGridview2.Items.Add("측정스텝 추가", null, cm_AddMeasuringStep_Click);
            cmGridview2.Items.Add("스텝 삭제", null, cm_RemoveStep_Click);

            gridScheduleStep.ContextMenuStrip = cmGridview2;


            repocbDynoMode.Items.Add("N");
            repocbDynoMode.Items.Add("T");
            repocbDynoMode.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;


            repocbEngineMode.Items.Add("α");
            repocbEngineMode.Items.Add("N");
            repocbEngineMode.Items.Add("T");
            repocbEngineMode.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;


            repocbOutputItem.Items.Add("-");
            repocbOutputItem.Items.Add("MAXRPM");
            repocbOutputItem.Items.Add("IDLERPM");
            repocbOutputItem.Items.Add("MAXPOWER");
            repocbOutputItem.Items.Add("IDLEOILPRESSURE");
            repocbOutputItem.Items.Add("FULLOILPRESSURE");
            repocbOutputItem.Items.Add("SMOKE");
            repocbOutputItem.Items.Add("FUELCONSUMPTION");
            repocbOutputItem.Items.Add("JBOILPRESSURE");
            repocbOutputItem.Items.Add("LOADSMOKE");
            repocbOutputItem.Items.Add("FASMOKE");
            repocbOutputItem.Items.Add("MAXTORQUE");
            repocbOutputItem.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            repocbOutputItem.EditValueChanged += RepositoryItemComboBox_EditValueChanged;

            repocbMeasuringChannel.Items.Add("-");
            repocbMeasuringChannel.Items.Add("스피드");
            repocbMeasuringChannel.Items.Add("수정토크");
            repocbMeasuringChannel.Items.Add("수정출력");
            repocbMeasuringChannel.Items.Add("오일압력_ECU");
            repocbMeasuringChannel.Items.Add("수정연료소비량");
            repocbMeasuringChannel.Items.Add("매연");
            repocbMeasuringChannel.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
        }
        private void cm_CopySchedule_Click(object sender, EventArgs e)
        {
            Copied = scheduleFiles[gridView1.FocusedRowHandle].Clone() as ScheduleFile;

            var cmItem = sender as ToolStripItem;
            var cm = cmItem.GetCurrentParent();
            cm.Items[5].Enabled = true;
        }
        private void cm_PasteSchedule_Click(object sender, EventArgs e)
        {
            if (Copied == null)
            {
                return;
            }

            if (CheckOverlapScheduleFile(Copied.ScheduleName))
                scheduleFiles.Add(Copied);

            gridView1.BeginUpdate();
            gridView1.EndUpdate();

            Copied = null;

            var cmItem = sender as ToolStripItem;
            cmItem.Enabled = false;

        }

        private void RepositoryItemComboBox_EditValueChanged(object sender, EventArgs e)
        {
            var combobox = sender as ComboBoxEdit;
            int rowhandle = gridView2.FocusedRowHandle;

            if (combobox.Text == "-")
            {
                gridView2.SetRowCellValue(rowhandle, gridView2.Columns["MeasuringChannel"], "-");

                string[] ColList = new string[5] { "MeasuringStartTime", "MeasuringEndTime", "Unit", "LowValue", "HighValue" };

                for (int i = 0; i < ColList.Length; i++)
                {
                    gridView2.SetRowCellValue(rowhandle, gridView2.Columns[ColList[i]], "");
                }
            }
            else
            {
                string[] ColList = new string[4] { "MeasuringStartTime", "MeasuringEndTime", "LowValue", "HighValue" };

                for (int i = 0; i < ColList.Length; i++)
                {
                    gridView2.SetRowCellValue(rowhandle, gridView2.Columns[ColList[i]], "0");
                }
            }

            //scheduleFiles[gridView1.FocusedRowHandle].IsEdited = true;
        }

        void ListFileOnGridview()
        {
            DirectoryInfo d = new DirectoryInfo(ScheduleFilePath);
            FileInfo[] Files = d.GetFiles("*.csv");

            var sortedFiles = new DirectoryInfo(ScheduleFilePath).GetFiles()
                                                  .OrderBy(f => f.CreationTime)
                                                  .ToList();
            int index = 0;

            foreach (FileInfo file in sortedFiles)
            {
                string[] textLines = File.ReadAllLines(file.FullName, Encoding.Default);

                scheduleFiles.Add(new ScheduleFile(++index, textLines[0].Split(',')[0], textLines[0].Split(',')[1]));

                for (int i = 3; i < textLines.Length; i++)
                {
                    ScheduleStep newStep = new ScheduleStep();
                    string[] textStep = textLines[i].Split(',');
                    newStep.Step = textStep[0];
                    newStep.StepName = textStep[1];
                    newStep.DynoMode = textStep[2];
                    newStep.DynoSetValue = textStep[3];
                    newStep.EngineMode = textStep[4];
                    newStep.EngineSetValue = textStep[5];
                    newStep.Ramp = textStep[6];
                    newStep.Time = textStep[7];
                    newStep.OutputItem = textStep[8];
                    newStep.MeasuringChannel = textStep[9];
                    newStep.MeasuringStartTime = textStep[10];
                    newStep.MeasuringEndTime = textStep[11];
                    newStep.Unit = textStep[12];
                    newStep.LowValue = textStep[13];
                    newStep.HighValue = textStep[14];
                    newStep.Exhaust = textStep[15];

                    scheduleFiles[index - 1].IsEdited = false;

                    scheduleFiles[index - 1].scheduleSteps.Add(newStep);
                }
            }
        }
        void MakeFileFromList()
        {
            for (int i = 0; i < scheduleFiles.Count; i++)
            {
                string FileName = ScheduleFilePath + "\\" + scheduleFiles[i].ScheduleName + ".csv";

                if (File.Exists(FileName) && !scheduleFiles[i].IsEdited)//로드되고 수정 X. 그냥 둔다.
                {
                    continue;
                }
                else if (File.Exists(FileName) && scheduleFiles[i].IsEdited)//로드되고 수정 O. 파일 지우고 새로 써줌.
                {
                    File.Delete(FileName);
                }

                File.AppendAllText(FileName, scheduleFiles[i].ScheduleName + "," + scheduleFiles[i].Description + "\r\n", Encoding.Default);
                File.AppendAllText(FileName, "==============" + "\r\n");
                File.AppendAllText(FileName, "스텝,스텝이름,동력계 모드,동력계 셋값,엔진 모드,엔진 셋값,가감시간,유지시간,출력 항목,측정 채널,시작,종료,단위,Low,High,배기개도량" + "\r\n", Encoding.Default);
                string context = "";

                for (int j = 0; j < scheduleFiles[i].scheduleSteps.Count; j++)
                {
                    context += scheduleFiles[i].scheduleSteps[j].Step + ","
                        + scheduleFiles[i].scheduleSteps[j].StepName + ","
                        + scheduleFiles[i].scheduleSteps[j].DynoMode + ","
                        + scheduleFiles[i].scheduleSteps[j].DynoSetValue + ","
                        + scheduleFiles[i].scheduleSteps[j].EngineMode + ","
                        + scheduleFiles[i].scheduleSteps[j].EngineSetValue + ","
                        + scheduleFiles[i].scheduleSteps[j].Ramp + ","
                    + scheduleFiles[i].scheduleSteps[j].Time + ","
                    + scheduleFiles[i].scheduleSteps[j].OutputItem + ","
                    + scheduleFiles[i].scheduleSteps[j].MeasuringChannel + ","
                    + scheduleFiles[i].scheduleSteps[j].MeasuringStartTime + ","
                    + scheduleFiles[i].scheduleSteps[j].MeasuringEndTime + ","
                    + scheduleFiles[i].scheduleSteps[j].Unit + ","
                    + scheduleFiles[i].scheduleSteps[j].LowValue + ","
                    + scheduleFiles[i].scheduleSteps[j].HighValue + ","
                    + scheduleFiles[i].scheduleSteps[j].Exhaust + "\r\n";
                }

                File.AppendAllText(FileName, context, Encoding.Default);
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {

            using (AddNewScheduleFile form = new AddNewScheduleFile())
            {
                if (form.ShowDialog() == DialogResult.OK)
                {
                    if (CheckOverlapScheduleFile(form.FileName))
                        scheduleFiles.Add(new ScheduleFile(scheduleFiles.Count + 1, form.FileName, form.Description));
                }
            }
            gridView1.BeginUpdate();

            gridView1.FocusedRowHandle = gridView1.DataRowCount - 1;

            gridView1_RowCountChanged(null, null);

            gridView1.EndUpdate();
        }
        private bool CheckOverlapScheduleFile(string fileName)
        {

            foreach (ScheduleFile scheduleFile in scheduleFiles)
            {
                if (scheduleFile.ScheduleName == fileName)
                {
                    XtraMessageBox.Show("<size=14>이미 같은 이름의 스케쥴이 존재합니다.</size>", "<size=14>에러</size>", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }

            return true;
        }
        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (gridView1.RowCount == 0)
            {
                return;
            }

            gridView1.BeginUpdate();
            string FileName = ScheduleFilePath + "\\" + scheduleFiles[gridView1.FocusedRowHandle].ScheduleName + ".csv";
            File.Delete(FileName);
            scheduleFiles.RemoveAt(gridView1.FocusedRowHandle);
            gridView1.EndUpdate();

            gridView1_RowCountChanged(null, null);
        }
        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (gridView1.RowCount == 0)
            {
                return;
            }

            using (AddNewScheduleFile form = new AddNewScheduleFile(scheduleFiles[gridView1.FocusedRowHandle].ScheduleName, scheduleFiles[gridView1.FocusedRowHandle].Description))
            {
                if (form.ShowDialog() == DialogResult.OK)
                {
                    string oldFileName = "";
                    string newFileName = "";
                    if (scheduleFiles[gridView1.FocusedRowHandle].ScheduleName != form.FileName || scheduleFiles[gridView1.FocusedRowHandle].Description != form.Description)
                    {
                        scheduleFiles[gridView1.FocusedRowHandle].IsRenamed = true;
                        oldFileName = scheduleFiles[gridView1.FocusedRowHandle].ScheduleName;
                    }

                    scheduleFiles[gridView1.FocusedRowHandle].ScheduleName = form.FileName;
                    scheduleFiles[gridView1.FocusedRowHandle].Description = form.Description;

                    oldFileName = ScheduleFilePath + "\\" + oldFileName + ".csv";
                    newFileName = ScheduleFilePath + "\\" + form.FileName + ".csv";

                    if (scheduleFiles[gridView1.FocusedRowHandle].IsRenamed && File.Exists(oldFileName))
                    {
                        if (oldFileName != newFileName)
                            File.Move(oldFileName, newFileName);

                        string newText = form.FileName + "," + form.Description;
                        lineChanger(newText, newFileName, 1);
                    }
                }
            }
            gridView1.BeginUpdate();
            gridView1.EndUpdate();
        }
        static void lineChanger(string newText, string fileName, int line_to_edit)
        {
            string[] arrLine = File.ReadAllLines(fileName, Encoding.Default);
            arrLine[line_to_edit - 1] = newText;
            File.WriteAllLines(fileName, arrLine, Encoding.Default);
        }
        private void gridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (gridView1.RowCount != 0)
            {
                gridScheduleStep.BeginUpdate();
                gridScheduleStep.DataSource = scheduleFiles[gridView1.FocusedRowHandle].scheduleSteps;
                if (scheduleFiles[gridView1.FocusedRowHandle].scheduleSteps.Count == 0)
                {
                    scheduleFiles[gridView1.FocusedRowHandle].scheduleSteps.Add(new ScheduleStep("1", "NewStep"));
                }
                gridScheduleStep.EndUpdate();

                gridScheduleStep.Visible = true;
            }
            else
            {
                
            }
        }

        private void gridView1_RowCountChanged(object sender, EventArgs e)
        {
            if(gridView1.RowCount == 0)
            {
                gridScheduleStep.Visible = false;

                gridScheduleStep.BeginUpdate();
                gridScheduleStep.DataSource = null;
                gridScheduleStep.EndUpdate();
            }
            else if (gridView1.RowCount == 1)
            {
                gridScheduleStep.BeginUpdate();
                gridScheduleStep.DataSource = scheduleFiles[0].scheduleSteps;
                if (scheduleFiles[0].scheduleSteps.Count == 0)
                {
                    scheduleFiles[0].scheduleSteps.Add(new ScheduleStep("1", "NewStep"));
                }
                gridScheduleStep.EndUpdate();

                gridScheduleStep.Visible = true;
            }
            else if(gridView1.RowCount > 1)
            {
                gridScheduleStep.Visible = true;
            }
        }
        private void cm_AddStep_Click(object sender, EventArgs e)
        {
            gridView2.BeginDataUpdate();
            if (gridView2.RowCount == 0)
            {
                ScheduleStep scheduleStep = new ScheduleStep("1", "NewStep");
                scheduleStep.MakeDefaultNormalStep();
                scheduleFiles[gridView1.FocusedRowHandle].scheduleSteps.Add(scheduleStep);
            }
            else
            {
                ScheduleStep scheduleStep = new ScheduleStep((scheduleFiles[gridView1.FocusedRowHandle].StepCount + 1).ToString(), "NewStep");
                scheduleStep.MakeDefaultNormalStep();
                scheduleFiles[gridView1.FocusedRowHandle].scheduleSteps.Insert(gridView2.FocusedRowHandle + 1, scheduleStep);
                scheduleFiles[gridView1.FocusedRowHandle].MeasuringStepCount = 0;
                scheduleFiles[gridView1.FocusedRowHandle].RenewIndex();
            }
            scheduleFiles[gridView1.FocusedRowHandle].IsEdited = true;
            gridView2.EndDataUpdate();
        }
        private void cm_AddMeasuringStep_Click(object sender, EventArgs e)
        {
            gridView2.BeginDataUpdate();
            if (gridView2.RowCount == 0)
            {
                XtraMessageBox.Show("<size=14>최소한 한 개의 스텝은 존재해야 측정스텝을 추가할 수 있습니다.</size>", "<size=14>에러</size>", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                scheduleFiles[gridView1.FocusedRowHandle].MeasuringStepCount++;
                scheduleFiles[gridView1.FocusedRowHandle].scheduleSteps.Insert(gridView2.FocusedRowHandle + 1, new ScheduleStep($"{scheduleFiles[gridView1.FocusedRowHandle].StepCount + 1}_{scheduleFiles[gridView1.FocusedRowHandle].MeasuringStepCount}", ""));
                scheduleFiles[gridView1.FocusedRowHandle].RenewIndex();

                scheduleFiles[gridView1.FocusedRowHandle].IsEdited = true;
            }
            gridView2.EndDataUpdate();
        }
        private void cm_RemoveStep_Click(object sender, EventArgs e)
        {
            gridView2.BeginDataUpdate();
            scheduleFiles[gridView1.FocusedRowHandle].scheduleSteps.RemoveAt(gridView2.FocusedRowHandle);
            scheduleFiles[gridView1.FocusedRowHandle].RenewIndex();

            scheduleFiles[gridView1.FocusedRowHandle].IsEdited = true;
            gridView2.EndDataUpdate();
        }
        private void gridView2_ShowingEditor(object sender, CancelEventArgs e)
        {
            var view = sender as GridView;
            string fieldName = view.FocusedColumn.FieldName;
            int rowindex = view.FocusedRowHandle;

            if (fieldName == "Step")
            { e.Cancel = true; }

            if (gridView2.GetRowCellValue(rowindex, gridView2.Columns[0]).ToString().Contains("_"))
            {
                string[] disableEditingColumns = { "StepName", "DynoMode", "DynoSetValue", "EngineMode", "EngineSetValue", "Ramp", "Time", "Exhaust" };

                if (disableEditingColumns.Contains(fieldName))
                {
                    e.Cancel = true;
                }
            }

            if (gridView2.GetRowCellValue(rowindex, gridView2.Columns["OutputItem"]).ToString() == "-")
            {
                string[] disableEditingColumns = { "MeasuringChannel" };

                if (disableEditingColumns.Contains(fieldName))
                {
                    e.Cancel = true;
                }
            }

            if (gridView2.GetRowCellValue(rowindex, gridView2.Columns["OutputItem"]).ToString() == "-" && gridView2.GetRowCellValue(rowindex, gridView2.Columns["MeasuringChannel"]).ToString() == "-")
            {
                string[] disableEditingColumns = { "MeasuringStartTime", "MeasuringEndTime", "Unit", "LowValue", "HighValue" };

                if (disableEditingColumns.Contains(fieldName))
                {
                    e.Cancel = true;
                }
            }
        }

        private void MakeScheduleList_FormClosed(object sender, FormClosedEventArgs e)
        {
            MakeFileFromList();
        }

        private void gridView2_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            scheduleFiles[gridView1.FocusedRowHandle].IsEdited = true;
        }


    }

    public class ScheduleFile : ICloneable
    {
        int index;
        string scheduleName;
        string description;
        int measuringStepCount = 0;
        bool isEdited = true;
        bool isRenamed = false;

        public int Index { get => index; set => index = value; }
        public string ScheduleName { get => scheduleName; set => scheduleName = value; }
        public string Description { get => description; set => description = value; }
        public int StepCount
        {
            get
            {
                int result = 0;

                foreach (ScheduleStep item in scheduleSteps)
                {
                    int i = 0;
                    bool canConvert = int.TryParse(item.Step, out i);

                    if (canConvert)
                    {
                        result++;
                    }
                }

                return result;
            }
        }
        public int MeasuringStepCount { get => measuringStepCount; set => measuringStepCount = value; }
        public bool IsEdited { get => isEdited; set => isEdited = value; }
        public bool IsRenamed { get => isRenamed; set => isRenamed = value; }

        public List<ScheduleStep> scheduleSteps = new List<ScheduleStep>();

        public void RenewIndex()
        {
            int stepIndex = 0;
            int measuringIndex = 1;
            foreach (ScheduleStep ss in scheduleSteps)
            {
                int result = 0;
                int.TryParse(ss.Step, out result);

                if (result != 0)
                {
                    stepIndex++;
                    ss.Step = stepIndex.ToString();
                    measuringIndex = 1;
                }
                else
                {
                    ss.Step = stepIndex.ToString() + "_" + measuringIndex.ToString();
                    measuringIndex++;
                }
            }
        }
        public ScheduleFile(int index, string scheduleName, string description)
        {
            this.Index = index;
            this.ScheduleName = scheduleName;
            this.Description = description;
        }

        public ScheduleFile()
        {

        }

        public object Clone()
        {
            ScheduleFile cloned = new ScheduleFile();

            cloned.index = this.index;
            cloned.scheduleName = this.scheduleName + " - 복사본";
            cloned.description = this.description;
            cloned.measuringStepCount = this.measuringStepCount;

            cloned.scheduleSteps = this.scheduleSteps.ConvertAll(o => o);

            return cloned;
        }
    }

    public class ScheduleStep
    {
        string step;
        string stepName;
        string dynoMode;
        string dynoSetValue;
        string engineMode;
        string engineSetValue;
        string ramp;
        string time;
        string outputItem = "-";
        string measuringChannel = "-";
        string measuringStartTime;
        string measuringEndTime;
        string unit;
        string lowValue;
        string highValue;
        string exhaust;

        public ScheduleStep()
        {

        }
        public ScheduleStep(string step, string stepName)
        {
            this.step = step;
            this.stepName = stepName;
        }

        public void MakeDefaultNormalStep()
        {
            this.dynoMode = "N";
            this.dynoSetValue = "0";
            this.engineMode = "α";
            this.engineSetValue = "0";
            this.ramp = "0";
            this.time = "0";
            this.exhaust = "0";
        }

        public string Step { get => step; set => step = value; }
        public string StepName { get => stepName; set => stepName = value; }
        public string DynoMode { get => dynoMode; set => dynoMode = value; }
        public string DynoSetValue { get => dynoSetValue; set => dynoSetValue = value; }
        public string EngineMode { get => engineMode; set => engineMode = value; }
        public string EngineSetValue { get => engineSetValue; set => engineSetValue = value; }
        public string Ramp { get => ramp; set => ramp = value; }
        public string Time { get => time; set => time = value; }
        public string OutputItem { get => outputItem; set => outputItem = value; }
        public string MeasuringChannel { get => measuringChannel; set => measuringChannel = value; }
        public string MeasuringStartTime
        {
            get => measuringStartTime;
            set
            {
                measuringStartTime = value;
                //if (measuringStartTime == null || Ramp == null || Time == null)
                //{
                //    return;
                //}

                //if (int.Parse(measuringStartTime) > int.Parse(Ramp) + int.Parse(Time))
                //{
                //    measuringStartTime = "0";
                //    XtraMessageBox.Show("<size=14>측정 시작시간은 가감시간 + 유지시간보다 길 수 없습니다.</size>", "<size=14>에러</size>", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //}
            }
        }
        public string MeasuringEndTime
        {
            get => measuringEndTime;
            set
            {
                measuringEndTime = value;
                //if (measuringStartTime == null || Ramp == null || Time == null)
                //{
                //    return;
                //}

                //if (int.Parse(measuringEndTime) > int.Parse(Ramp) + int.Parse(Time))
                //{
                //    measuringEndTime = "0";
                //    XtraMessageBox.Show("<size=14>측정 종료시간은 가감시간 + 유지시간보다 길 수 없습니다.</size>", "<size=14>에러</size>", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //}
            }
        }
        public string Unit { get => unit; set => unit = value; }
        public string LowValue { get => lowValue; set => lowValue = value; }
        public string HighValue { get => highValue; set => highValue = value; }
        public string Exhaust { get => exhaust; set => exhaust = value; }

    }
}