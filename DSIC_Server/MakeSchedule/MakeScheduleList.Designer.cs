﻿namespace DSIC_Server.MakeSchedule
{
    partial class MakeScheduleList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MakeScheduleList));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnEdit = new DevExpress.XtraEditors.SimpleButton();
            this.btnRemove = new DevExpress.XtraEditors.SimpleButton();
            this.btnAdd = new DevExpress.XtraEditors.SimpleButton();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridScheduleList = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.ColIndex = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColScheduleName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridScheduleStep = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.ColStep = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColStepName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColDynoSetValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColEngineSetValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColRamp = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColOutputItem = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repocbOutputItem = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.ColMeasuringChannel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repocbMeasuringChannel = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.ColMeasuringStartTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColMeasuringEndTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColUnit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColLowValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColHighValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColExhaust = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColDynoMode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repocbDynoMode = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.ColEngineMode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repocbEngineMode = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemCheckedComboBoxEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridScheduleList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridScheduleStep)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repocbOutputItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repocbMeasuringChannel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repocbDynoMode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repocbEngineMode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.btnEdit);
            this.panelControl1.Controls.Add(this.btnRemove);
            this.panelControl1.Controls.Add(this.btnAdd);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1519, 62);
            this.panelControl1.TabIndex = 0;
            // 
            // btnEdit
            // 
            this.btnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEdit.Appearance.Options.UseFont = true;
            this.btnEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnEdit.ImageOptions.Image")));
            this.btnEdit.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnEdit.Location = new System.Drawing.Point(279, 12);
            this.btnEdit.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(107, 37);
            this.btnEdit.TabIndex = 2;
            this.btnEdit.Text = "편집";
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnRemove
            // 
            this.btnRemove.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRemove.Appearance.Options.UseFont = true;
            this.btnRemove.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnRemove.ImageOptions.Image")));
            this.btnRemove.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnRemove.Location = new System.Drawing.Point(149, 12);
            this.btnRemove.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(107, 37);
            this.btnRemove.TabIndex = 1;
            this.btnRemove.Text = "제거";
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.Appearance.Options.UseFont = true;
            this.btnAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.ImageOptions.Image")));
            this.btnAdd.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnAdd.Location = new System.Drawing.Point(19, 12);
            this.btnAdd.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(107, 37);
            this.btnAdd.TabIndex = 0;
            this.btnAdd.Text = "추가";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 62);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.gridScheduleList);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.gridScheduleStep);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1519, 558);
            this.splitContainerControl1.SplitterPosition = 231;
            this.splitContainerControl1.TabIndex = 1;
            // 
            // gridScheduleList
            // 
            this.gridScheduleList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridScheduleList.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridScheduleList.Location = new System.Drawing.Point(0, 0);
            this.gridScheduleList.MainView = this.gridView1;
            this.gridScheduleList.Name = "gridScheduleList";
            this.gridScheduleList.Size = new System.Drawing.Size(1519, 231);
            this.gridScheduleList.TabIndex = 0;
            this.gridScheduleList.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView1.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView1.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView1.Appearance.Row.Options.UseTextOptions = true;
            this.gridView1.Appearance.Row.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.ColIndex,
            this.ColScheduleName,
            this.ColDescription});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFullFocus;
            this.gridView1.GridControl = this.gridScheduleList;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.OptionsFilter.AllowFilterEditor = false;
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsFind.Behavior = DevExpress.XtraEditors.FindPanelBehavior.Search;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView1_FocusedRowChanged);
            this.gridView1.RowCountChanged += new System.EventHandler(this.gridView1_RowCountChanged);
            // 
            // ColIndex
            // 
            this.ColIndex.Caption = "Index";
            this.ColIndex.FieldName = "Index";
            this.ColIndex.Name = "ColIndex";
            this.ColIndex.Visible = true;
            this.ColIndex.VisibleIndex = 0;
            this.ColIndex.Width = 79;
            // 
            // ColScheduleName
            // 
            this.ColScheduleName.Caption = "스케쥴 이름";
            this.ColScheduleName.FieldName = "ScheduleName";
            this.ColScheduleName.Name = "ColScheduleName";
            this.ColScheduleName.Visible = true;
            this.ColScheduleName.VisibleIndex = 1;
            this.ColScheduleName.Width = 364;
            // 
            // ColDescription
            // 
            this.ColDescription.Caption = "설명";
            this.ColDescription.FieldName = "Description";
            this.ColDescription.Name = "ColDescription";
            this.ColDescription.Visible = true;
            this.ColDescription.VisibleIndex = 2;
            this.ColDescription.Width = 481;
            // 
            // gridScheduleStep
            // 
            this.gridScheduleStep.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridScheduleStep.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridScheduleStep.Location = new System.Drawing.Point(0, 0);
            this.gridScheduleStep.MainView = this.gridView2;
            this.gridScheduleStep.Name = "gridScheduleStep";
            this.gridScheduleStep.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repocbOutputItem,
            this.repocbMeasuringChannel,
            this.repositoryItemCheckedComboBoxEdit1,
            this.repocbDynoMode,
            this.repocbEngineMode});
            this.gridScheduleStep.Size = new System.Drawing.Size(1519, 317);
            this.gridScheduleStep.TabIndex = 1;
            this.gridScheduleStep.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            this.gridScheduleStep.Visible = false;
            // 
            // gridView2
            // 
            this.gridView2.ActiveFilterEnabled = false;
            this.gridView2.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 11.25F);
            this.gridView2.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView2.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView2.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView2.Appearance.Row.Options.UseTextOptions = true;
            this.gridView2.Appearance.Row.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.ColStep,
            this.ColStepName,
            this.ColDynoSetValue,
            this.ColEngineSetValue,
            this.ColRamp,
            this.ColTime,
            this.ColOutputItem,
            this.ColMeasuringChannel,
            this.ColMeasuringStartTime,
            this.ColMeasuringEndTime,
            this.ColUnit,
            this.ColLowValue,
            this.ColHighValue,
            this.ColExhaust,
            this.ColDynoMode,
            this.ColEngineMode});
            this.gridView2.GridControl = this.gridScheduleStep;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsCustomization.AllowSort = false;
            this.gridView2.OptionsFilter.AllowAutoFilterConditionChange = DevExpress.Utils.DefaultBoolean.False;
            this.gridView2.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView2.OptionsFilter.AllowFilterEditor = false;
            this.gridView2.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.gridView2.OptionsFilter.AllowMRUFilterList = false;
            this.gridView2.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.gridView2.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView2_ShowingEditor);
            this.gridView2.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridView2_CellValueChanged);
            // 
            // ColStep
            // 
            this.ColStep.Caption = "스텝";
            this.ColStep.FieldName = "Step";
            this.ColStep.Name = "ColStep";
            this.ColStep.OptionsFilter.AllowAutoFilter = false;
            this.ColStep.OptionsFilter.AllowFilter = false;
            this.ColStep.Visible = true;
            this.ColStep.VisibleIndex = 0;
            this.ColStep.Width = 57;
            // 
            // ColStepName
            // 
            this.ColStepName.Caption = "스텝 이름";
            this.ColStepName.FieldName = "StepName";
            this.ColStepName.Name = "ColStepName";
            this.ColStepName.OptionsFilter.AllowAutoFilter = false;
            this.ColStepName.OptionsFilter.AllowFilter = false;
            this.ColStepName.Visible = true;
            this.ColStepName.VisibleIndex = 1;
            this.ColStepName.Width = 127;
            // 
            // ColDynoSetValue
            // 
            this.ColDynoSetValue.Caption = "동력계 셋값";
            this.ColDynoSetValue.FieldName = "DynoSetValue";
            this.ColDynoSetValue.Name = "ColDynoSetValue";
            this.ColDynoSetValue.OptionsFilter.AllowAutoFilter = false;
            this.ColDynoSetValue.OptionsFilter.AllowFilter = false;
            this.ColDynoSetValue.Visible = true;
            this.ColDynoSetValue.VisibleIndex = 3;
            this.ColDynoSetValue.Width = 102;
            // 
            // ColEngineSetValue
            // 
            this.ColEngineSetValue.Caption = "엔진 셋값";
            this.ColEngineSetValue.FieldName = "EngineSetValue";
            this.ColEngineSetValue.Name = "ColEngineSetValue";
            this.ColEngineSetValue.OptionsFilter.AllowAutoFilter = false;
            this.ColEngineSetValue.OptionsFilter.AllowFilter = false;
            this.ColEngineSetValue.Visible = true;
            this.ColEngineSetValue.VisibleIndex = 5;
            this.ColEngineSetValue.Width = 89;
            // 
            // ColRamp
            // 
            this.ColRamp.Caption = "가감시간";
            this.ColRamp.FieldName = "Ramp";
            this.ColRamp.Name = "ColRamp";
            this.ColRamp.OptionsFilter.AllowAutoFilter = false;
            this.ColRamp.OptionsFilter.AllowFilter = false;
            this.ColRamp.Visible = true;
            this.ColRamp.VisibleIndex = 6;
            this.ColRamp.Width = 67;
            // 
            // ColTime
            // 
            this.ColTime.Caption = "유지시간";
            this.ColTime.FieldName = "Time";
            this.ColTime.Name = "ColTime";
            this.ColTime.OptionsFilter.AllowAutoFilter = false;
            this.ColTime.OptionsFilter.AllowFilter = false;
            this.ColTime.Visible = true;
            this.ColTime.VisibleIndex = 7;
            this.ColTime.Width = 69;
            // 
            // ColOutputItem
            // 
            this.ColOutputItem.Caption = "출력 항목";
            this.ColOutputItem.ColumnEdit = this.repocbOutputItem;
            this.ColOutputItem.FieldName = "OutputItem";
            this.ColOutputItem.Name = "ColOutputItem";
            this.ColOutputItem.OptionsFilter.AllowAutoFilter = false;
            this.ColOutputItem.OptionsFilter.AllowFilter = false;
            this.ColOutputItem.Visible = true;
            this.ColOutputItem.VisibleIndex = 8;
            this.ColOutputItem.Width = 184;
            // 
            // repocbOutputItem
            // 
            this.repocbOutputItem.AutoHeight = false;
            this.repocbOutputItem.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repocbOutputItem.Name = "repocbOutputItem";
            // 
            // ColMeasuringChannel
            // 
            this.ColMeasuringChannel.Caption = "측정 채널";
            this.ColMeasuringChannel.ColumnEdit = this.repocbMeasuringChannel;
            this.ColMeasuringChannel.FieldName = "MeasuringChannel";
            this.ColMeasuringChannel.Name = "ColMeasuringChannel";
            this.ColMeasuringChannel.OptionsFilter.AllowAutoFilter = false;
            this.ColMeasuringChannel.OptionsFilter.AllowFilter = false;
            this.ColMeasuringChannel.Visible = true;
            this.ColMeasuringChannel.VisibleIndex = 9;
            this.ColMeasuringChannel.Width = 175;
            // 
            // repocbMeasuringChannel
            // 
            this.repocbMeasuringChannel.AutoHeight = false;
            this.repocbMeasuringChannel.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repocbMeasuringChannel.Name = "repocbMeasuringChannel";
            // 
            // ColMeasuringStartTime
            // 
            this.ColMeasuringStartTime.Caption = "시작";
            this.ColMeasuringStartTime.FieldName = "MeasuringStartTime";
            this.ColMeasuringStartTime.Name = "ColMeasuringStartTime";
            this.ColMeasuringStartTime.OptionsFilter.AllowAutoFilter = false;
            this.ColMeasuringStartTime.OptionsFilter.AllowFilter = false;
            this.ColMeasuringStartTime.Visible = true;
            this.ColMeasuringStartTime.VisibleIndex = 10;
            this.ColMeasuringStartTime.Width = 67;
            // 
            // ColMeasuringEndTime
            // 
            this.ColMeasuringEndTime.Caption = "종료";
            this.ColMeasuringEndTime.FieldName = "MeasuringEndTime";
            this.ColMeasuringEndTime.Name = "ColMeasuringEndTime";
            this.ColMeasuringEndTime.OptionsFilter.AllowAutoFilter = false;
            this.ColMeasuringEndTime.OptionsFilter.AllowFilter = false;
            this.ColMeasuringEndTime.Visible = true;
            this.ColMeasuringEndTime.VisibleIndex = 11;
            this.ColMeasuringEndTime.Width = 68;
            // 
            // ColUnit
            // 
            this.ColUnit.Caption = "단위";
            this.ColUnit.FieldName = "Unit";
            this.ColUnit.Name = "ColUnit";
            this.ColUnit.OptionsFilter.AllowAutoFilter = false;
            this.ColUnit.OptionsFilter.AllowFilter = false;
            this.ColUnit.Visible = true;
            this.ColUnit.VisibleIndex = 12;
            this.ColUnit.Width = 54;
            // 
            // ColLowValue
            // 
            this.ColLowValue.Caption = "Low";
            this.ColLowValue.FieldName = "LowValue";
            this.ColLowValue.Name = "ColLowValue";
            this.ColLowValue.OptionsFilter.AllowAutoFilter = false;
            this.ColLowValue.OptionsFilter.AllowFilter = false;
            this.ColLowValue.Visible = true;
            this.ColLowValue.VisibleIndex = 13;
            this.ColLowValue.Width = 61;
            // 
            // ColHighValue
            // 
            this.ColHighValue.Caption = "High";
            this.ColHighValue.FieldName = "HighValue";
            this.ColHighValue.Name = "ColHighValue";
            this.ColHighValue.OptionsFilter.AllowAutoFilter = false;
            this.ColHighValue.OptionsFilter.AllowFilter = false;
            this.ColHighValue.Visible = true;
            this.ColHighValue.VisibleIndex = 14;
            this.ColHighValue.Width = 63;
            // 
            // ColExhaust
            // 
            this.ColExhaust.Caption = "배기개도량";
            this.ColExhaust.FieldName = "Exhaust";
            this.ColExhaust.Name = "ColExhaust";
            this.ColExhaust.OptionsFilter.AllowAutoFilter = false;
            this.ColExhaust.OptionsFilter.AllowFilter = false;
            this.ColExhaust.Visible = true;
            this.ColExhaust.VisibleIndex = 15;
            this.ColExhaust.Width = 163;
            // 
            // ColDynoMode
            // 
            this.ColDynoMode.Caption = "동력계 모드";
            this.ColDynoMode.ColumnEdit = this.repocbDynoMode;
            this.ColDynoMode.FieldName = "DynoMode";
            this.ColDynoMode.Name = "ColDynoMode";
            this.ColDynoMode.OptionsFilter.AllowAutoFilter = false;
            this.ColDynoMode.OptionsFilter.AllowFilter = false;
            this.ColDynoMode.Visible = true;
            this.ColDynoMode.VisibleIndex = 2;
            this.ColDynoMode.Width = 89;
            // 
            // repocbDynoMode
            // 
            this.repocbDynoMode.AutoHeight = false;
            this.repocbDynoMode.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repocbDynoMode.Name = "repocbDynoMode";
            // 
            // ColEngineMode
            // 
            this.ColEngineMode.Caption = "엔진 모드";
            this.ColEngineMode.ColumnEdit = this.repocbEngineMode;
            this.ColEngineMode.FieldName = "EngineMode";
            this.ColEngineMode.Name = "ColEngineMode";
            this.ColEngineMode.OptionsFilter.AllowAutoFilter = false;
            this.ColEngineMode.OptionsFilter.AllowFilter = false;
            this.ColEngineMode.Visible = true;
            this.ColEngineMode.VisibleIndex = 4;
            this.ColEngineMode.Width = 82;
            // 
            // repocbEngineMode
            // 
            this.repocbEngineMode.AutoHeight = false;
            this.repocbEngineMode.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repocbEngineMode.Name = "repocbEngineMode";
            // 
            // repositoryItemCheckedComboBoxEdit1
            // 
            this.repositoryItemCheckedComboBoxEdit1.AutoHeight = false;
            this.repositoryItemCheckedComboBoxEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCheckedComboBoxEdit1.Name = "repositoryItemCheckedComboBoxEdit1";
            // 
            // MakeScheduleList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1519, 620);
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.panelControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MakeScheduleList";
            this.Text = "Schedule";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MakeScheduleList_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridScheduleList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridScheduleStep)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repocbOutputItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repocbMeasuringChannel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repocbDynoMode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repocbEngineMode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton btnEdit;
        private DevExpress.XtraEditors.SimpleButton btnRemove;
        private DevExpress.XtraEditors.SimpleButton btnAdd;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraGrid.GridControl gridScheduleList;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.GridControl gridScheduleStep;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn ColIndex;
        private DevExpress.XtraGrid.Columns.GridColumn ColScheduleName;
        private DevExpress.XtraGrid.Columns.GridColumn ColDescription;
        private DevExpress.XtraGrid.Columns.GridColumn ColStep;
        private DevExpress.XtraGrid.Columns.GridColumn ColStepName;
        private DevExpress.XtraGrid.Columns.GridColumn ColDynoSetValue;
        private DevExpress.XtraGrid.Columns.GridColumn ColEngineSetValue;
        private DevExpress.XtraGrid.Columns.GridColumn ColRamp;
        private DevExpress.XtraGrid.Columns.GridColumn ColTime;
        private DevExpress.XtraGrid.Columns.GridColumn ColOutputItem;
        private DevExpress.XtraGrid.Columns.GridColumn ColMeasuringChannel;
        private DevExpress.XtraGrid.Columns.GridColumn ColMeasuringStartTime;
        private DevExpress.XtraGrid.Columns.GridColumn ColMeasuringEndTime;
        private DevExpress.XtraGrid.Columns.GridColumn ColUnit;
        private DevExpress.XtraGrid.Columns.GridColumn ColLowValue;
        private DevExpress.XtraGrid.Columns.GridColumn ColHighValue;
        private DevExpress.XtraGrid.Columns.GridColumn ColExhaust;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repocbOutputItem;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repocbMeasuringChannel;
        private DevExpress.XtraGrid.Columns.GridColumn ColDynoMode;
        private DevExpress.XtraGrid.Columns.GridColumn ColEngineMode;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repocbDynoMode;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repocbEngineMode;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit repositoryItemCheckedComboBoxEdit1;
    }
}