﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DSIC_Server.MakeSchedule;
using DSIC_Server.SendSchedule;
using DSIC_Server.WriteEngineInfo;

namespace DSIC_Server
{
    public partial class Main : DevExpress.XtraEditors.XtraForm
    {
        public static MSSQLDatabase database = new MSSQLDatabase();
        public Main()
        {
            InitializeComponent();

            Properties.Settings.Default.Save();

            //if(!database.DBConnect("10.32.35.229", "1433", "dacos", "socad", "DHIM"))
            if (!database.DBConnect(Properties.Settings.Default.DBIPAddress, Properties.Settings.Default.DBPort, Properties.Settings.Default.DBUserID, Properties.Settings.Default.DBPwd, Properties.Settings.Default.DBTableName))
            {
                XtraMessageBox.Show("<size=14>데이터베이스 서버 연결에 실패했습니다.</size>", "<size=14>에러</size>", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                XtraMessageBox.Show("<size=14>데이터베이스 서버 연결 성공.</size>", "<size=14>DB 연결 성공</size>", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

        private void Main_FormClosing(object sender, FormClosingEventArgs e)
        {

        }

        private void btnMakeSchedule_Click(object sender, EventArgs e)
        {
            MakeScheduleList form = new MakeScheduleList();
            form.Show();
        }

        private void btnSendSchedule_Click(object sender, EventArgs e)
        {
            SendScheduleFile form = new SendScheduleFile();
            form.Show();
        }

        private void btnMakeEngineInfo_Click(object sender, EventArgs e)
        {
            WriteEngineInfomation form = new WriteEngineInfomation();
            form.Show();
        }

        private void btnSettings_Click(object sender, EventArgs e)
        {
            Settings form = new Settings();
            form.Show();
        }

        private void btnSearchData_Click(object sender, EventArgs e)
        {
            SearchEngineInformation form = new SearchEngineInformation();
            form.Show();
        }
    }
}