﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Diagnostics;

namespace DSIC_Server
{
    public class MSSQLDatabase
    {

        string connectionString = "server = 10.32.88.229, 1433; uid = dacos; pwd = socad; database = DHIM;Connection Timeout=5";

        public bool DBConnect(string ip, string port, string userid, string pwd, string dbName)
        {
            connectionString = $"server = {ip},{port}; uid = {userid}; pwd = {pwd}; database = {dbName};Connection Timeout=5";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    SqlCommand sqlCmd = new SqlCommand();
                    sqlCmd.Connection = connection;
                    connection.Open();

                    return true;
                }
                catch (SqlException)
                {
                    return false;
                }
            }
        }

        //아래 스트링은 내가 만든 임의. 테이블네임은 스트링만 바꾸면 되고 칼럼은 스트링 바꾸고 칼럼 필드네임 일치하게 변경해줘야됨

        //DB Table Name
        string suffixTableName = Properties.Resources.EngineInfoTableName;
        string cnfoTableName = Properties.Resources.CnfoTableName;
        //Column Name 
        string suffix = Properties.Resources.ColSuffix;
        string model = Properties.Resources.ColModel;
        string engineCategory = Properties.Resources.ColEngineCatergory;
        string engineType = Properties.Resources.ColEngineType;
        string generationDate = Properties.Resources.ColGenerationDate;
        string engineConfigFilePath = Properties.Resources.ColEngineConfigFilePath;
        string diagnosticsFilePath = Properties.Resources.ColDiagnosticsFilePath;
        string flashConfigFilePath = Properties.Resources.ColFlashConfigFilePath; // 내가 추가한 테이블 맵 파일
        string scheduleFilePath = Properties.Resources.ColScheduleFilePath;
        string purpose = Properties.Resources.ColForWhat;
        string turboCharger = Properties.Resources.ColTurboCharger;
        string usage = Properties.Resources.ColUsage;
        string description = Properties.Resources.ColDescription;
        string flashConfigFilePathOriginal = Properties.Resources.ColFlashConfigFilePathOriginal; //원본 테이블 맵 파일

        public Tuple<DataSet, string> SearchDBResult(string suffix_content, string model_content, string startInsdate_content, string endInsdate_content, string scheduleFile_content)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand sqlCmd = new SqlCommand();
                sqlCmd.Connection = connection;
                connection.Open();

                sqlCmd.CommandText = "select ROW_NUMBER() OVER(ORDER BY INSDATE) as 'Index' , EGNO as 엔진번호, Suffix, Model, Insdate as 시험날짜, Sch_Name as '스케쥴 파일', ECUFileName as '맵 파일', CategorySmall as '엔진 구분', CategoryLarge as '엔진 형태', TurboCharger, " +
                    "APS_rpm as '최대출력 RPM 기준', APS_min as '최대출력 RPM Min', APS_max as '최대출력 RPM Max', APS as '최대출력 RPM 측정값', " +
                    "ATQ_rpm as '최대토크 RPM 기준', ATQ_min as '최대토크 Min', ATQ_max as '최대토크 Max', ATQ as '최대토크 측정값', " +
                    "ASMOK_rpm as '정격매연 RPM 기준', ASMOK_max as '정격매연 Max', ASMOK as '정격매연 측정값', " +
                    "AFASMOK_max as 'FA매연 Max', AFASMOK as 'FA매연 측정값', " +
                    "ALOILP_min as 'Idle 오일압 Min', ALOILP_max as 'Idle 오일압 Max', ALOILP as 'Idle 오일압 측정값', " +
                    "AFOILP_min as 'Full 오일압 Min', AFOILP_max as 'Full 오일압 Max', AFOILP as 'Full 오일압 측정값', " +
                    "AJOILP_min as 'JB 오일압 Min', AJOILP_max as 'JB 오일압 Max', AJOILP as 'JB 오일압 측정값', " +
                    "ALRPM_min as 'Idle RPM Min', ALRPM_max as 'Idle RPM Max', ALRPM as 'Idle RPM 측정값', " +
                    "AFRPM_min as 'Max RPM Min', AFRPM_max as 'Max RPM Max', AFRPM as 'Max RPM 측정값', " +
                    "ABSFC_rpm as '출력연비 RPM 기준', ABSFC_max as '출력연비 Max', ABSFC as '출력연비 측정값', " +
                    "AIRTEMP as 흡입온도, RELHUMID as 상대습도, AIRPRE as 대기압, KNUM as K계수, FTEMPIN as 연료온도, WTEMP as 냉각수출구온도, FuelDensity as 연료비중, Coeff_fm as '배기압력', " +
                    "CELLNAME as '셀 이름', INSPECT as '작업자'  from t_TestResult where Model=@model and INSDATE Between @startInsdate and @endInsdate";
                if (suffix_content != "")
                {
                    sqlCmd.CommandText += " and Suffix = @suffix";
                }
                if (scheduleFile_content != "")
                {
                    sqlCmd.CommandText += " and Sch_Name = @scheduleFile";
                }

                sqlCmd.Parameters.AddWithValue("@model", model_content);
                sqlCmd.Parameters.AddWithValue("@startInsdate", startInsdate_content);
                sqlCmd.Parameters.AddWithValue("@endInsdate", endInsdate_content);
                sqlCmd.Parameters.AddWithValue("@suffix", suffix_content);
                sqlCmd.Parameters.AddWithValue("@scheduleFile", scheduleFile_content);

                SqlDataAdapter da = new SqlDataAdapter(sqlCmd); //select 구문이 들어감
                DataSet ds = new DataSet();
                try
                {
                    da.Fill(ds, suffixTableName);
                }
                catch (SqlException)
                {
                    Debug.Print("Select DB 과정에서 SQL 에러 발생.");
                    return null;
                }

                return Tuple.Create<DataSet, string>(ds, suffixTableName);
            }
        }
        public DataRow SearchDBResultByEGNOforResultReport(string EGNO_content)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand sqlCmd = new SqlCommand();
                sqlCmd.Connection = connection;
                connection.Open();

                sqlCmd.CommandText = "select EGNO, MODEL, Sch_Name,TurboCharger,ECUFileName, AIRTEMP, RELHUMID, AIRPRE, KNUM, FTEMPIN, WTEMP, FuelDensity, Coeff_fm, APS_rpm, APS_min, APS_max, APS, ATQ_RPM, ATQ_min, ATQ_max, ATQ, ASMOK_rpm, ASMOK_max, ASMOK, AFASMOK_max, AFASMOK, ALOILP_min, ALOILP_max, ALOILP, AFOILP_min, AFOILP_max, AFOILP, AJOILP_min, AJOILP_max, AJOILP, ALRPM_min, ALRPM_max, ALRPM, AFRPM_min, AFRPM_max, AFRPM, ABSFC_rpm, ABSFC_max, ABSFC" +
                    " from t_TestResult where EGNO = @egno";
                sqlCmd.Parameters.AddWithValue("@egno", EGNO_content);

                SqlDataAdapter da = new SqlDataAdapter(sqlCmd); //select 구문이 들어감
                DataSet ds = new DataSet();
                try
                {
                    da.Fill(ds, suffixTableName);
                }
                catch (SqlException)
                {
                    Debug.Print("Select DB 과정에서 SQL 에러 발생.");
                    return null;
                }

                return ds.Tables[0].Rows[0];
            }
        }
        public Tuple<DataSet, string> DBSelectEngineInfo()
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    SqlCommand sqlCmd = new SqlCommand();
                    sqlCmd.Connection = connection;
                    connection.Open();

                    sqlCmd.CommandText = $"select " +
                    $"{suffixTableName}.{suffix}," +
                    $"{suffixTableName}.{model}," +
                    $"{suffixTableName}.{engineCategory}," +
                    $"{suffixTableName}.{engineType}," +
                    $"{suffixTableName}.{generationDate}," +
                    $"{cnfoTableName}.{engineConfigFilePath}," +
                    $"{cnfoTableName}.{diagnosticsFilePath}," +
                    $"{cnfoTableName}.{flashConfigFilePath}," +
                    $"{cnfoTableName}.{scheduleFilePath}," +
                    $"{suffixTableName}.{purpose}," +
                    $"{suffixTableName}.{turboCharger}," +
                    $"{suffixTableName}.{description}" +
                    $" from {suffixTableName} inner join {cnfoTableName} on {suffixTableName}.{suffix} = {cnfoTableName}.{suffix} and {suffixTableName}.{flashConfigFilePathOriginal} = {cnfoTableName}.{flashConfigFilePath}";

                    //sqlCmd.CommandText = $"select " +
                    //    $"{cnfoTableName}.{flashConfigFilePath} " +
                    //    $"{suffixTableName}.{model}," +
                    //    $" from {suffixTableName} inner join {cnfoTableName} on {suffixTableName}.{suffix} = {cnfoTableName}.{suffix}";

                    //select 이름 안맞으면 에러남. where 쓰려면 param붙여서 사용.


                    SqlDataAdapter da = new SqlDataAdapter(sqlCmd); //select 구문이 들어감
                    DataSet ds = new DataSet();

                    da.Fill(ds, suffixTableName);

                    return Tuple.Create<DataSet, string>(ds, suffixTableName);
                }
                catch (SqlException)
                {
                    Debug.Print("Select DB 과정에서 SQL 에러 발생.");
                    return null;
                }


                //sqlCmd.CommandText = $"select result.test, result.test2, sub.test10" +
                //    $" from Result inner join sub on Result.test = sub.test";

                ////select 이름 안맞으면 에러남.
                //SqlDataAdapter da = new SqlDataAdapter(sqlCmd); //select 구문이 들어감
                //DataSet ds = new DataSet();
                //da.Fill(ds, tableName);

            }
            //return Tuple.Create<DataSet, string>(ds, tableName);
        }

        public bool InsertEngineInfo(string suffix_content, string model_content, string engineCategory_content, string engineType_content, string generationDate_content, string engineConfigFilePath_content, string diagnosticsFilePath_content, string flashConfigFilePath_content, string scheduleFilePath_content, string forWhat_content, string turboCharger_content, string description_content)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand sqlCmd = new SqlCommand();
                    sqlCmd.Connection = connection;
                    connection.Open();

                    sqlCmd.CommandText = $"if exists(" +
                       $"select {suffix} from {suffixTableName} where {suffix} = @suffix and {flashConfigFilePathOriginal} = @flashConfigpath) " +
                       $"begin " +
                       $"select 99 cnt " +
                       $"end " +
                       $"else " +
                       $"begin " +
                       $"insert into {cnfoTableName} ({suffix},{engineConfigFilePath},{diagnosticsFilePath},{flashConfigFilePath},{scheduleFilePath})" +
                       $"values (@suffix,@engineconfigpath,@diagnosticspath,@flashConfigpath,@schedulepath) " +
                       $"end ";
                    sqlCmd.Parameters.AddWithValue("@suffix", suffix_content);
                    sqlCmd.Parameters.AddWithValue("@engineconfigpath", engineConfigFilePath_content);
                    sqlCmd.Parameters.AddWithValue("@diagnosticspath", diagnosticsFilePath_content);
                    sqlCmd.Parameters.AddWithValue("@flashConfigpath", flashConfigFilePath_content);
                    sqlCmd.Parameters.AddWithValue("@schedulepath", scheduleFilePath_content);

                    SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
                    DataSet ds = new DataSet();
                    try
                    {
                        da.Fill(ds, suffixTableName);

                        if (ds.Tables.Count == 0)
                        {
                            //정상 Insert 성공
                        }
                        else
                        {
                            //중복 존재
                            return false;
                        }
                    }
                    catch (SqlException)
                    {
                        Debug.Print("Select DB 과정에서 SQL 에러 발생.");
                        return false;
                    }
                }

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand sqlCmd = new SqlCommand();
                    sqlCmd.Connection = connection;
                    connection.Open();
                    sqlCmd.CommandText = $"insert into {suffixTableName} ({suffix},{model},{engineCategory},{engineType},{generationDate},{purpose},{turboCharger},{description},{flashConfigFilePathOriginal}) " +
                        $"values (@param1,@param2,@param3,@param4,@param5,@param6,@param7,@param8,@param9)";
                    sqlCmd.Parameters.AddWithValue("@param1", suffix_content);
                    sqlCmd.Parameters.AddWithValue("@param2", model_content);
                    sqlCmd.Parameters.AddWithValue("@param3", engineCategory_content);
                    sqlCmd.Parameters.AddWithValue("@param4", engineType_content);
                    sqlCmd.Parameters.AddWithValue("@param5", generationDate_content);
                    sqlCmd.Parameters.AddWithValue("@param6", forWhat_content);
                    if (turboCharger_content == "YES")
                    {
                        turboCharger_content = "true";
                    }
                    else
                    {
                        turboCharger_content = "false";
                    }
                    sqlCmd.Parameters.AddWithValue("@param7", turboCharger_content);
                    sqlCmd.Parameters.AddWithValue("@param8", description_content);
                    sqlCmd.Parameters.AddWithValue("@param9", flashConfigFilePath_content);
                    int rowNum = sqlCmd.ExecuteNonQuery();
                    connection.Close();
                }

                return true;
            }
            catch (SqlException)
            {
                Debug.Print("Insert DB 과정에서 SQL 에러 발생.");
                return false;
            }
        }
        public bool DeleteEngineInfo(string suffix_content, string flashConfigFilePath_content)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand sqlCmd = new SqlCommand();
                    sqlCmd.Connection = connection;
                    connection.Open();

                    sqlCmd.CommandText = $"delete from {suffixTableName} where {suffix} = @suffix and  {flashConfigFilePathOriginal} = @flashConfig";
                    sqlCmd.Parameters.AddWithValue("@suffix", suffix_content);
                    sqlCmd.Parameters.AddWithValue("@flashConfig", flashConfigFilePath_content);

                    int rowNum = sqlCmd.ExecuteNonQuery();
                    connection.Close();
                }

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand sqlCmd = new SqlCommand();
                    sqlCmd.Connection = connection;
                    connection.Open();

                    sqlCmd.CommandText = $"delete from {cnfoTableName} where {suffix} = @suffix and  {flashConfigFilePath} = @flashConfig";
                    sqlCmd.Parameters.AddWithValue("@suffix", suffix_content);
                    sqlCmd.Parameters.AddWithValue("@flashConfig", flashConfigFilePath_content);

                    int rowNum = sqlCmd.ExecuteNonQuery();
                    connection.Close();
                }
                return true;
            }
            catch (SqlException)
            {
                Debug.Print("Delete DB 과정에서 SQL 에러 발생.");
                return false;
            }
        }
        public bool UpdateEngineInfo(string suffix_content, string model_content, string engineCategory_content, string engineType_content, string generationDate_content, string engineConfigFilePath_content, string diagnosticsFilePath_content, string flashConfigFilePath_content, string scheduleFilePath_content, string forWhat_content, string turboCharger_content, string description_content)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand sqlCmd = new SqlCommand();
                    sqlCmd.Connection = connection;
                    connection.Open();

                    sqlCmd.CommandText = $"update {suffixTableName} set " +
                    $"{model} = @model," +
                    $"{engineCategory} = @engineCategory," +
                    $"{engineType} = @engineType," +
                    $"{purpose} = @purpose," +
                    $"{turboCharger} = @turboCharger," +
                    $"{description} = @description " +
                    $"from {suffixTableName} where {suffix} = @suffix and {flashConfigFilePathOriginal} = @flashConfig";

                    sqlCmd.Parameters.AddWithValue("@suffix", suffix_content);
                    sqlCmd.Parameters.AddWithValue("@model", model_content);
                    sqlCmd.Parameters.AddWithValue("@engineCategory", engineCategory_content);
                    sqlCmd.Parameters.AddWithValue("@engineType", engineType_content);
                    sqlCmd.Parameters.AddWithValue("@purpose", forWhat_content);
                    if (turboCharger_content == "YES")
                    {
                        turboCharger_content = "true";
                    }
                    else
                    {
                        turboCharger_content = "false";
                    }
                    sqlCmd.Parameters.AddWithValue("@turboCharger", turboCharger_content);
                    sqlCmd.Parameters.AddWithValue("@description", description_content);
                    sqlCmd.Parameters.AddWithValue("@flashConfig", flashConfigFilePath_content);

                    int rowNum = sqlCmd.ExecuteNonQuery();
                    connection.Close();
                }
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand sqlCmd = new SqlCommand();
                    sqlCmd.Connection = connection;
                    connection.Open();

                    sqlCmd.CommandText = $"update {cnfoTableName} set " +
                    $"{engineConfigFilePath} = @engineConfigFile," +
                    $"{diagnosticsFilePath} = @engineDiagnostics," +
                    $"{flashConfigFilePath} = @flashConfig," +
                    $"{scheduleFilePath} = @scheduleFile " +
                    $"from {cnfoTableName} where {suffix} = @suffix and {flashConfigFilePath} = @flashConfig";

                    sqlCmd.Parameters.AddWithValue("@suffix", suffix_content);
                    sqlCmd.Parameters.AddWithValue("@engineConfigFile", engineConfigFilePath_content);
                    sqlCmd.Parameters.AddWithValue("@engineDiagnostics", diagnosticsFilePath_content);
                    sqlCmd.Parameters.AddWithValue("@flashConfig", flashConfigFilePath_content);
                    sqlCmd.Parameters.AddWithValue("@scheduleFile", scheduleFilePath_content);

                    int rowNum = sqlCmd.ExecuteNonQuery();
                    connection.Close();
                }
                return true;
            }

            catch (SqlException)
            {
                Debug.Print("Update DB 과정에서 SQL 에러 발생.");
                return false;
            }
        }
    }
}
